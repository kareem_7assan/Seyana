package com.aait.seyana.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Adapter.SliderAdapter;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Dialogs.BenfiterCodeDialog;
import com.aait.seyana.Models.GetOrder.GetOrderModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aya on 10/11/2017.
 */

public class EndOrderFragment extends BaseFragment {

    @BindView(R.id.tv_order)
    TextView tv_order;
    @BindView(R.id.addresse)
    TextView addresse;
    @BindView(R.id.tvـphone)
    TextView tv_phone;
    @BindView(R.id.tvـaddresse)
    TextView tvـaddresse;
    @BindView(R.id.btn_end)
    TextView btn_end;
    @BindView(R.id.pager)
    ViewPager mPager;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    private static int currentPage = 0;

    String id = "";
    String name = "";
    GetOrderModel model;

    @Override
    protected int getViewId() {
        return R.layout.fragment_end_order;
    }

    @Override
    protected void init(View view) {
        id = getArguments().getString("id");
        name = getArguments().getString("name");
        //toaster.makeToast(name);
        getOrder();

    }

    @Override
    protected String SetTitle() {
        return getString(R.string.user_order_title) + " " + id;
    }

    @Override
    protected AppCompatActivity ActivityType() {
        return new MainActivity();
    }

    @Override
    public void onClick(View view) {

    }

    @OnClick(R.id.btn_end)
    void End() {
        if (name != null) {
            CustomeProgressDialog.onStart(getContext(), getString(R.string.please_wait));
            RetroWeb.getClient().create(ServiceApi.class).onSendOrderData(id).enqueue(new Callback<GetOrderModel>() {
                @Override
                public void onResponse(Call<GetOrderModel> call, Response<GetOrderModel> response) {
                    CustomeProgressDialog.onFinish();
                    BenfiterCodeDialog benfiterCodeDialog = new BenfiterCodeDialog();
                    benfiterCodeDialog.setTargetFragment(EndOrderFragment.this, 300);
                    Bundle args = new Bundle();
                    args.putString("id", id);
                    benfiterCodeDialog.setArguments(args);
                    benfiterCodeDialog.show(getActivity().getSupportFragmentManager(), "Dialog");
                }

                @Override
                public void onFailure(Call<GetOrderModel> call, Throwable t) {
                    CustomeProgressDialog.onFinish();

                }
            });

        } else {
            toaster.makeToast(getString(R.string.finish_order_done));
            MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(OrdersFragment.class.getName()).replace(R.id.content, new OrdersFragment()).commit();

        }
    }

    @OnClick(R.id.addresse)
    public void onADDresse() {

        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putString("name", name);
        bundle.putString("lat", model.getData().getLat());
        bundle.putString("lng", model.getData().getLng());
        bundle.putString("address", model.getData().getAddress());
        bundle.putString("phone", model.getData().getPhone() + "");
        bundle.putInt("state",1);
        Map fragment = new Map();
        fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(Map.class.getName()).replace(R.id.content, fragment).commit();

    }

    public void getOrder() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getOrderDetails(id)
                .enqueue(new Callback<GetOrderModel>() {

                    @Override
                    public void onResponse(Call<GetOrderModel> call, Response<GetOrderModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1")) {
                            CustomeProgressDialog.onFinish();
                            model = response.body();
                            tv_order.setText(response.body().getData().getDescription());
                            tv_phone.setText("" + model.getData().getPhone());
                            tvـaddresse.setText("" + model.getData().getAddress());
                            final String[] XMEN = new String[response.body().getData().getImages().size()];

                            for (int i = 0; i < response.body().getData().getImages().size(); i++)
                                XMEN[i] = response.body().getData().getImages().get(i).getName();

                            final ArrayList<String> XMENArray = new ArrayList<String>();
                            for (int i = 0; i < response.body().getData().getImages().size(); i++)
                                XMENArray.add(XMEN[i]);

                            mPager.setAdapter(new SliderAdapter(getActivity(), XMENArray));
                            indicator.setViewPager(mPager);

                            // Auto start of viewpager
                            final Handler handler = new Handler();
                            final Runnable Update = new Runnable() {
                                public void run() {
                                    if (currentPage == XMEN.length) {
                                        currentPage = 0;
                                    }
                                    mPager.setCurrentItem(currentPage++, true);
                                }
                            };
                            Timer swipeTimer = new Timer();
                            swipeTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    handler.post(Update);
                                }
                            }, 2500, 2500);
                        } else {
                            toaster.makeToast(response.body().getKey());
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<GetOrderModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }

}
