package com.aait.seyana.Fragment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aait.seyana.Activities.LoginActivity;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Models.Login.LoginData;
import com.aait.seyana.Models.Login.LoginModel;
import com.aait.seyana.Models.RateModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aya on 10/11/2017.
 */

public class RatingFragment extends BaseFragment {

    @BindView(R.id.iv_photo)
    ImageView iv_photo;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.btn_send)
    Button btn_send;
    String rate;
    String id;
    @Override
    protected int getViewId() {
        return  R.layout.fragment_rate;
    }

    @Override
    protected void init(View view) {
        id=getArguments().getString("id");

    }

    @Override
    protected String SetTitle() {
        return getString(R.string.rate_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
        return new MainActivity();
    }

    @Override
    public void onClick(View view) {

    }

    @OnClick(R.id.btn_send)
    void Rate()
    {

            if (ratingBar.getRating() == 0){
                float rr = ratingBar.getRating()+1;
                rate = String.valueOf(rr);
            }else{
                rate = String.valueOf(ratingBar.getRating());
            }
                if (rate.equals("0")){
                    toaster.makeToast(getString(R.string.EnterRate));
                }else {
                    getRate();
                }
               // toaster.makeToast(rate);
     }



    public void getRate() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getRate(globalPreferences.getID(),id,rate)
                .enqueue(new Callback<RateModel>() {

                    @Override
                    public void onResponse(Call<RateModel> call, Response<RateModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            toaster.makeToast(response.body().getKey());
                            Intent intent = new Intent(getContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_left);
                        }
                        else
                        {
                            toaster.makeToast(response.body().getKey());
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<RateModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }



}
