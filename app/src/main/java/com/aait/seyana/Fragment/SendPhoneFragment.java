package com.aait.seyana.Fragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.aait.seyana.Activities.LoginActivity;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Dialogs.ActivateCodeDialog;
import com.aait.seyana.Models.ImagesModel;
import com.aait.seyana.Models.UserMakeOrder.UserMakeOrderModel;
import com.aait.seyana.Models.VisitorMakeOrderModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aya on 10/11/2017.
 */

public class SendPhoneFragment extends BaseFragment {

    @BindView(R.id.ed_phone)
    EditText ed_phone;
    @BindView(R.id.btn_send)
    Button btn_send;
    String service_id="";
    String desc="";
    String image="";
    String lat="";
    String lng="";
    String address="";
    List<ImagesModel> images;


    @Override
    protected int getViewId() {
        return  R.layout.fragment_send_phone;
    }

    @Override
    protected void init(View view) {
        service_id=getArguments().getString("id");
        desc=getArguments().getString("desc");
        image=getArguments().getString("image");
        lat=getArguments().getString("lat");
        lng=getArguments().getString("lng");
        address=getArguments().getString("address");
        images = new ArrayList<>();
        ImagesModel imagesModel=new ImagesModel();
        imagesModel.setImage(image);
        images.add(imagesModel);


    }

    @Override
    protected String SetTitle() {
        return getString(R.string.Add_Service_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
            return new MainActivity();

    }

    @Override
    public void onClick(View view) {

    }

    private boolean validationSend() {
        if (!validatePhone()) {
            return false;
        }
        return true;
    }

    private boolean validatePhone() {
        if (ed_phone.getText().toString().trim().isEmpty()) {
            ed_phone.setError(getString(R.string.please_enter_phone));
            Util.requestFocus(ed_phone, getActivity().getWindow());
            return false;
        }
        else if (ed_phone.getText().toString().length()<10)
        {
            ed_phone.setError(getString(R.string.enter_phone_ten));
            Util.requestFocus(ed_phone, getActivity().getWindow());
            return false;
        }
        return true;
    }
    @OnClick(R.id.btn_send)
    void Send()
    {
        if (validationSend())
        {
            getVisitorOrder();
        }
    }
    public void getVisitorOrder() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getVisitorOrder(service_id,desc,image,address,lat,lng,ed_phone.getText().toString())
                .enqueue(new Callback<VisitorMakeOrderModel>() {

                    @Override
                    public void onResponse(Call<VisitorMakeOrderModel> call, Response<VisitorMakeOrderModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            ActivateCodeDialog activateCodeDialog = new ActivateCodeDialog();
                            activateCodeDialog.setTargetFragment(SendPhoneFragment.this, 300);
                            Bundle args = new Bundle();
                            args.putString("id", response.body().getOrder_id()+"");
                            activateCodeDialog.setArguments(args);
                            activateCodeDialog.show(getActivity().getSupportFragmentManager(), "Dialog");

                        }
                        else
                        {
                            toaster.makeToast(response.body().getKey());
                            CustomeProgressDialog.onFinish();


                        }
                    }


                    @Override
                    public void onFailure(Call<VisitorMakeOrderModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }
}
