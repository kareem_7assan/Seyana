package com.aait.seyana.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.seyana.Activities.LoginActivity;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Models.ImagesModel;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Constant;
import com.aait.seyana.Utils.Util;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Aya on 10/11/2017.
 */

public class AddServiceFragment extends BaseFragment {

    @BindView(R.id.ed_state_desc)
    EditText ed_state_desc;
    @BindView(R.id.btn_order_service)
    Button btn_order_service;
    @BindView(R.id.iv_photo1)
    ImageView iv_photo1;
    @BindView(R.id.iv_photo2)
    ImageView iv_photo2;
    @BindView(R.id.iv_photo3)
    ImageView iv_photo3;
    @BindView(R.id.iv_photo4)
    ImageView iv_photo4;
    String service_id;
    String photo1 = "", photo2 = "", photo3 = "", photo4 = "";
    int mA=0;
    public String filePath, profileImageFilePath;
    List<ImagesModel> images;
    List<String> image;
    ImagesModel imagesModel;

    @Override
    protected int getViewId() {
        return  R.layout.fragment_add_service;
    }

    @Override
    protected void init(View view) {
        service_id=getArguments().getString("id");
        images = new ArrayList<>();
        image = new ArrayList<>();
        iv_photo2.setEnabled(false);
        iv_photo3.setEnabled(false);
        iv_photo4.setEnabled(false);


    }
    private boolean validatedesc() {
        if (ed_state_desc.getText().toString().trim().isEmpty()) {
            ed_state_desc.setError(getString(R.string.please_enter_state_desc));
            Util.requestFocus(ed_state_desc, getActivity().getWindow());
            return false;
        }
        return true;
    }
    public boolean ValidatePhoto1() {
        if (photo1.equals("")) {
            toaster.makeToast(getString(R.string.please_enter_service_state_photo));
            return false;
        }
        return true;
    }
    private boolean validationDesc() {
        if (!validatedesc()) {
            return false;
        }
        if (!ValidatePhoto1()) {
            return false;
        }
        return true;
    }
    @Override
    protected String SetTitle() {
        return getString(R.string.Add_Service_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
            return new MainActivity();

    }

    @Override
    public void onClick(View view) {

    }
    @OnClick(R.id.iv_photo1)
    void photo1()
    {
        mA=1;
        showPictureDialog();
        if (photo1.equals(""))
        iv_photo2.setEnabled(true);


    }

    @OnClick(R.id.iv_photo2)
    void photo2()
    {
            mA = 2;
            showPictureDialog();
        if (photo2.equals(""))
            iv_photo3.setEnabled(true);



    }

    @OnClick(R.id.iv_photo3)
    void photo3()
    {
        mA=3;
        showPictureDialog();
        if (photo3.equals(""))
            iv_photo4.setEnabled(true);


    }
    @OnClick(R.id.iv_photo4)
    void photo4()
    {
        mA=4;
        showPictureDialog();


    }
    @OnClick(R.id.btn_order_service)
    void order()
    {
        if (!photo1.equals("")) {
            image.add(photo1);
        }
        if (!photo2.equals("")) {
            image.add(photo2);
        }
        if (!photo3.equals("")) {
            image.add(photo3);
        }
        if (!photo4.equals("")) {
            image.add(photo4);
        }
        for (int i=0;i<image.size();i++) {

            //Log.e("image", image.get(i));
             imagesModel = new ImagesModel();
            imagesModel.setImage(image.get(i));
            images.add(imagesModel);
            Log.e("images1", new Gson().toJson(images));
        }

        if (validationDesc())
        {
            Bundle bundle = new Bundle();
            bundle.putString("id",service_id);
            bundle.putString("desc",ed_state_desc.getText().toString());
            bundle.putString("image",new Gson().toJson(images));
            MapAddressFragment fragment=new MapAddressFragment();
            fragment.setArguments(bundle);
            getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(MapAddressFragment.class.getName()).replace(R.id.content, fragment).commit();

        }
    }
    public void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle(getResources().getString(
                R.string.text_choosepicture));
        String[] pictureDialogItems = {
                getResources().getString(R.string.text_gallary),
                getResources().getString(R.string.text_camera)};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {

                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constant.PermissionCode.STORAGE);
                                    return;
                                } else {
                                    choosePhotoFromGallaryIntent();
                                }
                                break;
                            case 1:
                                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constant.PermissionCode.CAMERA);
                                    return;
                                } else {
                                    takePhotoFromCameraIntent();
                                }
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
    public void choosePhotoFromGallaryIntent() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, Constant.RequestCode.PHOTO_CHOOSE);
    }

    public void takePhotoFromCameraIntent() {
        Intent cameraIntent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, Constant.RequestCode.Take_PICTURE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.RequestCode.PHOTO_CHOOSE) {
            if (mA==1) {
                handleChoosePhotoFromGallery(data, iv_photo1);
            }else if (mA==2) {
                handleChoosePhotoFromGallery(data, iv_photo2);
            }else if (mA==3) {
                handleChoosePhotoFromGallery(data, iv_photo3);
            }
            else if (mA==4) {
                handleChoosePhotoFromGallery(data, iv_photo4);
            }
        } else if (requestCode == Constant.RequestCode.Take_PICTURE) {
            if (mA==1) {
                handleTakePhotoFromCamera(data, iv_photo1);
            }else if (mA==2) {
                handleTakePhotoFromCamera(data, iv_photo2);
            }
            else if (mA==3) {
                handleTakePhotoFromCamera(data, iv_photo3);
            }
            else if (mA==4) {
                handleTakePhotoFromCamera(data, iv_photo4);
            }
        }
    }
    public void handleChoosePhotoFromGallery(Intent data, ImageView mImageView) {
        if (data != null) {
            Uri uri = data.getData();
            profileImageFilePath = getRealPathFromURI(uri);
            filePath = profileImageFilePath;
            setPic(mImageView, filePath);
        }
    }
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    private void setPic(ImageView mImageView, String currentPath) {
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(currentPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(currentPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
        if (mA==1) {
            photo1 = Util.getEncoded64ImageStringFromBitmap(bitmap);
            /*image.add(photo1);
             imagesModel=new ImagesModel();
            imagesModel.setImage(photo1);*/
        }
        else if (mA==2) {
            photo2 = Util.getEncoded64ImageStringFromBitmap(bitmap);
           /* image.add(photo2);
             imagesModel=new ImagesModel();
            imagesModel.setImage(photo2);*/
        }
        else if (mA==3) {
            photo3 = Util.getEncoded64ImageStringFromBitmap(bitmap);
           /* image.add(photo3);
             imagesModel=new ImagesModel();
            imagesModel.setImage(photo3);*/
        }
        else if (mA==4) {
            photo4 = Util.getEncoded64ImageStringFromBitmap(bitmap);
         /*   image.add(photo4);
             imagesModel=new ImagesModel();
            imagesModel.setImage(photo4);*/
        }
        /*for (int i=0;i<image.size();i++) {
            imagesModel = new ImagesModel();
            imagesModel.setImage(image.get(i));
            images.add(imagesModel);
            Log.e("image", new Gson().toJson(image));
            //Log.e("images1", new Gson().toJson(images));


        }*/
    }
    private void handleTakePhotoFromCamera(Intent data ,ImageView mImageView) {
        Bundle extras = data.getExtras();
        Bitmap imageBitmap = (Bitmap) extras.get("data");
        Uri tempUri = getImageUri(getActivity(), imageBitmap);
        File finalFile = new File(getRealPathFromURI(tempUri));
        setPic(mImageView, finalFile.getAbsolutePath());
        if (mA==1) {
            photo1 = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
             /*imagesModel=new ImagesModel();
            imagesModel.setImage(photo1);
            images.add(imagesModel);*/
        }
        else if (mA==2) {
            photo2 = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
            /* imagesModel=new ImagesModel();
            imagesModel.setImage(photo2);
            images.add(imagesModel);*/
        }
        else if (mA==3) {
            photo3 = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
            /* imagesModel=new ImagesModel();
            imagesModel.setImage(photo3);
            images.add(imagesModel);*/
        }
        else if (mA==4) {
            photo4 = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
           /*  imagesModel=new ImagesModel();
            imagesModel.setImage(photo4);
            images.add(imagesModel);*/
        }

        /*for (int i=0;i<images.size();i++)

            Log.e("images1", new Gson().toJson(images));*/
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

}
