package com.aait.seyana.Fragment;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.aait.seyana.Activities.LoginActivity;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.R;
import com.aait.seyana.Utils.GlobalPreferences;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Aya on 10/11/2017.
 */

public class CongratulationCompleteFragment extends BaseFragment {

    @BindView(R.id.rl_cong)
    RelativeLayout rl_cong;
    GlobalPreferences globalPreferences;
    @Override
    protected int getViewId() {
        return  R.layout.activity_congratulation_complete;

    }

    @Override
    protected void init(View view) {
        globalPreferences=new GlobalPreferences(getActivity());
        Log.e("login",globalPreferences.getLoginStatus()+"");


    }

    @Override
    protected String SetTitle() {
        return getString(R.string.congratulation);
    }

    @Override
    protected AppCompatActivity ActivityType() {
            return new MainActivity();


    }

    @Override
    public void onClick(View view) {

    }
    @OnClick(R.id.rl_cong)
    void Cong()
    {
            getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(ServiceFragment.class.getName()).replace(R.id.content, new ServiceFragment()).commit();

    }
}
