package com.aait.seyana.Fragment;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.seyana.Activities.LoginActivity;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.R;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Aya on 10/11/2017.
 */

public class ServicesFragment extends BaseFragment {
    @BindView(R.id.iv_electric)
    ImageView iv_electric;
    @BindView(R.id.iv_air)
    ImageView iv_air;
    @BindView(R.id.iv_plumb)
    ImageView iv_plumb;
    @Override
    protected int getViewId() {
        return  R.layout.fragment_services;
    }

    @Override
    protected void init(View view) {

    }

    @Override
    protected String SetTitle() {
        return getString(R.string.services_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
            return new MainActivity();
           }

    @Override
    public void onClick(View view) {

    }
    @OnClick(R.id.iv_electric)
    void elec()
    {
       getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(AddServiceFragment.class.getName()).replace(R.id.content, new AddServiceFragment()).commit();

    }
    @OnClick(R.id.iv_plumb)
    void plum()
    {
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(AddServiceFragment.class.getName()).replace(R.id.content, new AddServiceFragment()).commit();

    }
    @OnClick(R.id.iv_air)
    void air()
    {
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(AddServiceFragment.class.getName()).replace(R.id.content, new AddServiceFragment()).commit();

    }
}
