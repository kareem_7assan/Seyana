package com.aait.seyana.Fragment;

import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.aait.seyana.Activities.LoginActivity;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Models.About.AboutModel;
import com.aait.seyana.Models.Terms.TermsModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aya on 10/11/2017.
 */

public class TermsFragment extends BaseFragment {

    @BindView(R.id.tv_terms)
    TextView tv_terms;
    @Override
    protected int getViewId() {
        return  R.layout.fragment_terms;
    }

    @Override
    protected void init(View view) {
        getTerms();

    }

    @Override
    protected String SetTitle() {
        return getString(R.string.terms_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
            return new MainActivity();

    }

    @Override
    public void onClick(View view) {

    }
    public void getTerms() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getTerms()
                .enqueue(new Callback<TermsModel>() {

                    @Override
                    public void onResponse(Call<TermsModel> call, Response<TermsModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            tv_terms.setText(Html.fromHtml(response.body().getData().getContent()));
                        }
                        else
                        {
                            CustomeProgressDialog.onFinish();
                        }
                    }
                    @Override
                    public void onFailure(Call<TermsModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }
}
