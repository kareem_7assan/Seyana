package com.aait.seyana.Fragment;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Adapter.ServicesAdapter;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Listners.INotificationFragment;
import com.aait.seyana.Models.OrderModel;
import com.aait.seyana.Models.Service.ServiceData;
import com.aait.seyana.Models.Service.ServiceModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Widget.CustomeProgressDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aya on 2/7/2017.
 */

public class ServiceFragment extends BaseFragment {

    @BindView(R.id.rv_services)
    RecyclerView rv_services;
    ServicesAdapter adapter;
    @BindView(R.id.notloading)
    ImageView notloading;
    @BindView(R.id.no_data)
    TextView no_data;
    private List<ServiceData> models;
    @BindView(R.id.activity_main_swipe_refresh_layout)
    SwipeRefreshLayout swiperefresh;

    @Override
    protected int getViewId() {
        return R.layout.fragment_service;
    }

    @Override
    protected void init(View view) {
        models = new ArrayList<>();

        adapter = new ServicesAdapter(getContext(),models);
        rv_services.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv_services.setAdapter(adapter);
        getServices();
        onRefresh();

    }

    @Override
    protected String SetTitle() {
        return getString(R.string.services_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
        return new MainActivity();
    }

    @Override
    public void onClick(View view) {

    }

    public static Fragment newInstance() {
        Fragment frag = new ServiceFragment();
        return frag;
    }

    public void getServices() {
        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getService().enqueue(new Callback<ServiceModel>() {
            @Override
            public void onResponse(Call<ServiceModel> call, Response<ServiceModel> response) {
                swiperefresh.setRefreshing(false);
                CustomeProgressDialog.onFinish();
                adapter = new ServicesAdapter(getContext(), response.body().getData());
                rv_services.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                rv_services.setAdapter(adapter);
                if (response.body().getData().size()==0)
                {
                    notloading.setVisibility(View.VISIBLE);
                    no_data.setVisibility(View.VISIBLE);


                   // Picasso.with(getActivity()).load(R.mipmap.no_data).into(notloading);
                    no_data.setText(getString(R.string.no_data));
                }
            }
            @Override
            public void onFailure(Call<ServiceModel> call, Throwable t) {
                customeProgressDialog.onFinish();
                notloading.setVisibility(View.VISIBLE);
                no_data.setVisibility(View.VISIBLE);
                swiperefresh.setRefreshing(false);
               // Picasso.with(getActivity()).load(R.mipmap.wifi_error).into(notloading);
                no_data.setText(getString(R.string.no_internent));

            }

        });
    }
    private void onRefresh() {
        swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getServices();
                        notloading.setVisibility(View.GONE);
                        no_data.setVisibility(View.GONE);
                    }
                }
        );
        swiperefresh.setColorSchemeResources(R.color.colorPrimary, R.color.orange, R.color.yellow);

    }
}
