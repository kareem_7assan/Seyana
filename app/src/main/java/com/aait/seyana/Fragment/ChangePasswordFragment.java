package com.aait.seyana.Fragment;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Models.ChangePasswordModel;
import com.aait.seyana.Models.ForgetPhoneModel;
import com.aait.seyana.Models.Profile.ProfileData;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aya on 10/11/2017.
 */

public class ChangePasswordFragment extends BaseFragment {

    @BindView(R.id.ed_old_password)
    EditText ed_old_password;
    @BindView(R.id.ed_new_password)
    EditText ed_new_password;
    @BindView(R.id.ed_confirm_password)
    EditText ed_confirm_password;
    @BindView(R.id.btn_save)
    Button btn_save;
    @Override
    protected int getViewId() {
        return  R.layout.fragment_change_password;
    }

    @Override
    protected void init(View view) {

    }

    @Override
    protected String SetTitle() {
        return getString(R.string.change_password_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
        return new MainActivity();
    }

    @Override
    public void onClick(View view) {

    }
    private boolean validationChange() {
        if (!validateOldPassword()) {
            return false;
        }
        if (!validateNewPassword()) {
            return false;
        }
        if (!validateConPassword()) {
            return false;
        }
        return true;
    }
    private boolean validateOldPassword() {
        if (ed_old_password.getText().toString().trim().isEmpty()) {
            ed_old_password.setError(getString(R.string.please_enter_old_password));
            Util.requestFocus(ed_old_password, getActivity().getWindow());
            return false;
        }
        return true;
    }
    private boolean validateNewPassword() {
        if (ed_new_password.getText().toString().trim().isEmpty()) {
            ed_new_password.setError(getString(R.string.please_enter_new_password));
            Util.requestFocus(ed_new_password, getActivity().getWindow());
            return false;
        }
        return true;
    }
    private boolean validateConPassword() {
        if (ed_confirm_password.getText().toString().trim().isEmpty()) {
            ed_confirm_password.setError(getString(R.string.please_enter_confirm));
            Util.requestFocus(ed_confirm_password, getActivity().getWindow());
            return false;
        }
        else
        {
            if (!ed_new_password.getText().toString().equals(ed_confirm_password.getText().toString()))
            {
                ed_confirm_password.setError(getString(R.string.please_enter_confirm_correct));
                Util.requestFocus(ed_confirm_password, getActivity().getWindow());
                return false;
            }
        }
        return true;
    }
    public void getChangePassword() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getChange(globalPreferences.getID(),ed_old_password.getText().toString(),ed_new_password.getText().toString())
                .enqueue(new Callback<ChangePasswordModel>() {

                    @Override
                    public void onResponse(Call<ChangePasswordModel> call, Response<ChangePasswordModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            toaster.makeToast(response.body().getMsg());
                            MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(ProfileFragment.class.getName()).replace(R.id.content, new ProfileFragment()).commit();

                        }
                        else
                        {
                            toaster.makeToast(response.body().getMsg());
                            CustomeProgressDialog.onFinish();
                        }
                    }
                    @Override
                    public void onFailure(Call<ChangePasswordModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }
    @OnClick(R.id.btn_save)
    void Save()
    {
        if (validationChange())
        {
            getChangePassword();
        }
    }
}
