package com.aait.seyana.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.seyana.Activities.LoginActivity;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Adapter.NavigationDrawerAdapter;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Listners.NavigationActionListner;
import com.aait.seyana.Models.MenuModel;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Constant;
import com.aait.seyana.Utils.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by mahmoud on 12/1/2017.
 */

public class NavigationFragment extends BaseFragment {
    public static String TAG = NavigationFragment.class.getSimpleName();
    @BindView(R.id.menu_recycle)
    RecyclerView menu_recycle;
    @BindView(R.id.civ_user_img)
    CircleImageView civ_user_img;
    @BindView(R.id.tv_username)
    TextView tv_username;

    @BindView(R.id.head)
    RelativeLayout head;
    ArrayList<MenuModel> data;
    NavigationDrawerAdapter drawerAdapter;
   static String order_number;
    static String notification_number;


    @Override
    protected int getViewId() {
        return R.layout.fragment_navigation_menu;
    }

    @Override
    protected void init(View view) {
         UpdateDrawer();

        if (globalPreferences.getLoginStatus()) {
            tv_username.setText(globalPreferences.getNAME());
            Picasso.with(getContext()).load(globalPreferences.getAVATAR()).resize(200,200)
                    .error(R.mipmap.logo_splash).into(civ_user_img);


        } else {
            head.setVisibility(View.GONE);
        }


    }

    @Override
    protected String SetTitle() {
        return null;
    }

    @Override
    protected AppCompatActivity ActivityType() {
        if (globalPreferences.getLoginStatus())
        return new MainActivity();
        else
            return new LoginActivity();

    }

    @Override
    public void onClick(View view) {
    }



    public void setMenuData() {
            data = new ArrayList<>();
        if (globalPreferences.getLoginStatus()) {
            if (globalPreferences.getLoginAs().equals(Constant.provider)) {
                data.add(new MenuModel(R.mipmap.home, getString(R.string.main)));
                data.add(new MenuModel(R.mipmap.service, getString(R.string.orders_title)));
                data.add(new MenuModel(R.mipmap.user_id, getString(R.string.profile)));
                data.add(new MenuModel(R.mipmap.about_us, getString(R.string.about)));
                data.add(new MenuModel(R.mipmap.setting, getString(R.string.settings)));
                data.add(new MenuModel(R.mipmap.share, getString(R.string.share)));
                data.add(new MenuModel(R.mipmap.phone_copy, getString(R.string.contact)));
                data.add(new MenuModel(R.mipmap.non, getString(R.string.terms)));
                data.add(new MenuModel(R.mipmap.edit, getString(R.string.suggests)));
                data.add(new MenuModel(R.mipmap.sign_out, getString(R.string.logout)));
            }
           else if (globalPreferences.getLoginAs().equals(Constant.benfiter)) {
                data.add(new MenuModel(R.mipmap.home, getString(R.string.main)));
                data.add(new MenuModel(R.mipmap.service, getString(R.string.my_orders)));
                data.add(new MenuModel(R.mipmap.user_id, getString(R.string.profile)));
                data.add(new MenuModel(R.mipmap.about_us, getString(R.string.about)));
                data.add(new MenuModel(R.mipmap.setting, getString(R.string.settings)));
                data.add(new MenuModel(R.mipmap.share, getString(R.string.share)));
                data.add(new MenuModel(R.mipmap.phone_copy, getString(R.string.contact)));
                data.add(new MenuModel(R.mipmap.non, getString(R.string.terms)));
                data.add(new MenuModel(R.mipmap.edit, getString(R.string.suggests)));
                data.add(new MenuModel(R.mipmap.sign_out, getString(R.string.logout)));
            }
        }
        else
        {
            data.add(new MenuModel(R.mipmap.home, getString(R.string.main)));
            data.add(new MenuModel(R.mipmap.sign_in, getString(R.string.login_title)));
            data.add(new MenuModel(R.mipmap.about_us, getString(R.string.about)));
            data.add(new MenuModel(R.mipmap.setting, getString(R.string.settings)));
            data.add(new MenuModel(R.mipmap.share, getString(R.string.share)));
            data.add(new MenuModel(R.mipmap.phone_copy, getString(R.string.contact)));
            data.add(new MenuModel(R.mipmap.non, getString(R.string.terms)));
        }


        menu_recycle.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        drawerAdapter = new NavigationDrawerAdapter((AppCompatActivity) getActivity(), data, navigationActionListner);
        menu_recycle.setAdapter(drawerAdapter);
    }


    NavigationActionListner navigationActionListner = new NavigationActionListner() {

        @Override
        public void onMain() {
            replaceFragment(new NowOrdersFragment());
        }

        @Override
        public void onService() {
            replaceFragment(new ServicesFragment());
        }
        @Override
        public void onCompletedOrder() {
            replaceFragment(new OrdersFragment());
        }
        @Override
        public void onMyOrders() {
            replaceFragment(new MyOrdersFragment());
        }
        @Override
        public void onProfile() {
            replaceFragment(new ProfileFragment());
        }

        @Override
        public void onAbout() {
            replaceFragment(new AboutFragment());
        }

        @Override
        public void onSettings() {
            replaceFragment(new SettingFragment());
        }

        @Override
        public void onShare() {

            Util.ShareApp(getActivity());

        }

        @Override
        public void onContact() {
            replaceFragment(new ContactFragment());
        }

        @Override
        public void onTerms() {
            replaceFragment(new TermsFragment());
        }

        @Override
        public void onSuggests() {
            replaceFragment(new SuggestsFragment());
        }

        @Override
        public void onLogout() {
            logout(getActivity());
        }

        @Override
        public void onLogin() {
            replaceFragment(new LoginFragment());
          /*  Intent intent = new Intent(getActivity(), LoginActivity.class);
            getActivity().startActivity(intent);*/
        }

    };

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        boolean exist = false;
            for (int i = 0; i < MainActivity.activity.getSupportFragmentManager().getBackStackEntryCount(); i++) {
                if (MainActivity.activity.getSupportFragmentManager().getBackStackEntryAt(i).getName() != null)
                    if (MainActivity.activity.getSupportFragmentManager().getBackStackEntryAt(i).getName().equals(backStateName)) {
                        exist = true;
                        break;
                    }
            }
            if (!exist) {
                MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(backStateName).replace(R.id.content, fragment).commit();
            } else
                MainActivity.activity.getSupportFragmentManager().popBackStackImmediate(backStateName, 0);

    }

    public void logout(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        //Setting message manually and performing action on button click
        builder.setMessage(getString(R.string.logout_message))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes_answer), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        globalPreferences.LogOut();
                        globalPreferences.storeLoginStatus(false);
                        MainActivity.activity.finish();
                        //  context.startActivity(new Intent(context, LoginActivity.class));
                    }
                })
                .setNegativeButton(getString(R.string.no_answer), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "Resume");
        setMenuData();
        if (globalPreferences.getLoginAs().equals(Constant.benfiter)||globalPreferences.getLoginAs().equals(Constant.provider)) {
            UpdateDrawer();
        }

    }


    public void UpdateDrawer() {
//        String name=globalPreferences.getNAME();
//        String image=globalPreferences.getAVATAR();
//            tv_username.setText(name);
//            if (!name.equals("")) {
//                Picasso.with(getActivity()).load(image).into(civ_user_img);
//            } else {
//                LitterAsImage litterAsImage = new LitterAsImage(getActivity());
//                if (!name.equals("")) {
//                    Bitmap bitmap = litterAsImage.litterToImage(R.drawable.yellow_oval, name);
//                    civ_user_img.setImageBitmap(bitmap);
//                }
//            }

    }

    public void drawerOpenClose() {
        if (globalPreferences.getLanguage().equals("ar") || globalPreferences.getLanguage().equals("en")) {
            MainActivity.drawerLayout.closeDrawer(Gravity.RIGHT);
        } else {
            MainActivity.drawerLayout.closeDrawer(Gravity.LEFT);
        }
    }
}