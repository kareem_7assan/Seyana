package com.aait.seyana.Fragment;

import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.seyana.Activities.LoginActivity;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Models.About.AboutModel;
import com.aait.seyana.Models.ChangePasswordModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.GlobalPreferences;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aya on 10/11/2017.
 */

public class AboutFragment extends BaseFragment {

    @BindView(R.id.tv_about)
    TextView tv_about;
    GlobalPreferences globalPreferences;
    @Override
    protected int getViewId() {
        return  R.layout.fragment_about;

    }

    @Override
    protected void init(View view) {
        globalPreferences=new GlobalPreferences(getActivity());
       // Log.e("login",globalPreferences.getLoginStatus()+"");
        getAbout();


    }

    @Override
    protected String SetTitle() {
        return getString(R.string.about_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
            return new MainActivity();


    }

    @Override
    public void onClick(View view) {

    }
    public void getAbout() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAbout()
                .enqueue(new Callback<AboutModel>() {

                    @Override
                    public void onResponse(Call<AboutModel> call, Response<AboutModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            tv_about.setText(Html.fromHtml(response.body().getData().getAbout_app()));
                        }
                        else
                        {
                            CustomeProgressDialog.onFinish();
                        }
                    }
                    @Override
                    public void onFailure(Call<AboutModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }
}
