package com.aait.seyana.Fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.GPS.GPSTracker;
import com.aait.seyana.GPS.GPSTrakerListner;
import com.aait.seyana.Models.MapModel;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Util;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by Aya on 8/1/2017.
 */

public class Map extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener, GPSTrakerListner {
    @BindView(R.id.map)
    MapView mapView;
    @BindView(R.id.btn_order_service)
    Button btn_order_service;
    @BindView(R.id.tv_phone)
    TextView tv_phone;
    @BindView(R.id.tv_location)
    TextView tv_location;
    public static GoogleMap googleMap;
    HashMap<Marker, MapModel> hmap1;
    HashMap<Marker, MapModel> hmap2;

    GPSTracker gps;
    Marker marker1;
    Marker marker2;
    MapModel mapModel;
    String id,address,lat,lng,phone,name;

    @Override
    protected int getViewId() {
        return R.layout.fragment_map;
    }
    @OnClick(R.id.btn_order_service)
    void getDirection() {

        Bundle bundle = new Bundle();

        bundle.putString("id", id);
        bundle.putString("name", name);
        EndOrderFragment fragment = new EndOrderFragment();
        fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(EndOrderFragment.class.getName()).replace(R.id.content, fragment).commit();


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void init(View view) {
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
        id=getArguments().getString("id");
        lat=getArguments().getString("lat");
        lng=getArguments().getString("lng");
        address=getArguments().getString("address");
        phone=getArguments().getString("phone");
        name=getArguments().getString("name");
        int state = (getArguments().getInt("state",0));
        if (state >0){
            btn_order_service.setVisibility(View.GONE);
        }
        tv_location.setText(getString(R.string.address)+" "+address);
        tv_phone.setText(getString(R.string.phone)+" "+phone);
        //toaster.makeToast(page);
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Initialize();

    }


    @Override
    protected String SetTitle() {
            return getString(R.string.user_order_title);


    }

    @Override
    protected AppCompatActivity ActivityType() {
            return new MainActivity();

    }


    @Override
    public void onClick(View view) {

    }

    public void Initialize() {
        hmap1 = new HashMap<>();
        hmap2 = new HashMap<>();

    }
/*
    private void AddMarkersToAjeer(MapModel model) {

        marker1 = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(-27.47093, 153.0235)));

            marker1.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.marker));

        hmap1.put(marker1, model);
    }*/

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        getLocaionWithPermission();
        onMarker();



    }
    public void onMarker(){
            marker1 = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
            marker1.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.marker));
        marker2 = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(Double.parseDouble(globalPreferences.getLAT()), Double.parseDouble(globalPreferences.getLNG()))));
        marker2.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.arrival_spot));
            hmap1.put(marker1, mapModel);
        hmap2.put(marker2, mapModel);
        LatLng latLng = new LatLng(Double.parseDouble(globalPreferences.getLAT()), Double.parseDouble(globalPreferences.getLNG()));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));

        onDrawDirection(globalPreferences.getLAT(),
                globalPreferences.getLNG(),
                "" + lat, "" + lng);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnInfoWindowClickListener(this);
    }

    /*private void putuserMarkerInMap(Double lat, Double log) {
        putMapMarker(lat, log);
    }

    public void putMapMarker(Double lat, Double log) {
        Log.e("LatLng:", "Lat: " + lat + " Lng: " + log);
        LatLng latLng = new LatLng(lat, log);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 8));
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(lat, log));
        marker.icon(BitmapDescriptorFactory
                .fromResource(R.mipmap.marker));
        googleMap.addMarker(marker);



    }*/

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (hmap1.containsKey(marker)) {

                MapModel model = hmap1.get(marker);


        } else {
            googleMap.setInfoWindowAdapter(null);
        }
        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
    }




    @Override
    public void onTrakerSuccess(Double lat, Double log) {

    }

    @Override
    public void onStartTraker() {

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void getLocaionWithPermission() {
        gps = new GPSTracker(getActivity(), this);
        if (canMakeSmores()) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                List<String> permissionsNeeded = new ArrayList<String>();
                final List<String> permissionsList = new ArrayList<String>();
                if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
                    permissionsNeeded.add("GPS");
                if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
                    permissionsNeeded.add("Location");

                if (permissionsList.size() > 0) {
                    if (permissionsNeeded.size() > 0) {
                        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                100);
                    }
                }
                Log.e("GPS", "1");
            } else {
                Log.e("GPS", "2");
                getCurrentLocation();
            }
        } else {
            Log.e("GPS", "3");
            getCurrentLocation();
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (getActivity().checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }

    // run time permission
    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);

    }

    void getCurrentLocation() {
        gps.getLocation();
        if (!gps.canGetLocation()) {
            showSettingsAlert();
            Log.e("Settings", "Settings");
        } else {
        }

    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 300);

            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
    public void drawPath(String result) {

        try {
            //Tranform the string into a json object

            final JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            List<LatLng> list = decodePoly(encodedString);

            for (int z = 0; z < list.size() - 1; z++) {
                LatLng src = list.get(z);
                LatLng dest = list.get(z + 1);
                googleMap.addPolyline(new PolylineOptions().add(new LatLng(src.latitude, src.longitude),
                        new LatLng(dest.latitude, dest.longitude)).width(20).color(getResources().getColor(R.color.colorPrimary)).geodesic(true));
            }

        } catch (JSONException e) {

        }
    }






    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)), (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    private void onDrawDirection(final String lat, final String lang, final String elat, final String elang) {


        final StringRequest request = new StringRequest(Request.Method.GET, Util.makeURL(
                Double.parseDouble(lat),
                Double.parseDouble(lang), Double.parseDouble(elat), Double.parseDouble(elang)
        ), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Util.onPrintLog(response);
                if(Map.this!=null) {
                    drawPath(response);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected java.util.Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap();


                return params;
            }
        };

        Log.e("url",request.getUrl());
        Volley.newRequestQueue(getActivity()).add(request);
    }



}