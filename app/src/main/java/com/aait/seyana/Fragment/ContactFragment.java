package com.aait.seyana.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.aait.seyana.Activities.LoginActivity;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Models.About.AboutModel;
import com.aait.seyana.Models.Contact.ContactModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aya on 10/11/2017.
 */

public class ContactFragment extends BaseFragment {

    @BindView(R.id.tv_mail)
    TextView tv_mail;
    @BindView(R.id.tv_number)
    TextView tv_number;
    @BindView(R.id.tv_facebook)
    TextView tv_facebook;
    @Override
    protected int getViewId() {
        return  R.layout.fragment_contactus;
    }

    @Override
    protected void init(View view) {
        getContact();
    }

    @Override
    protected String SetTitle() {
        return getString(R.string.contact_us_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
            return new MainActivity();

    }

    @Override
    public void onClick(View view) {

    }
    public void getContact() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getContact()
                .enqueue(new Callback<ContactModel>() {

                    @Override
                    public void onResponse(Call<ContactModel> call, Response<ContactModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            tv_mail.setText(response.body().getData().getEmail());
                            tv_number.setText(response.body().getData().getNumber());
                            tv_facebook.setText(response.body().getData().getFacebook());
                        }
                        else
                        {
                            toaster.makeToast(response.body().getKey());
                            CustomeProgressDialog.onFinish();
                        }
                    }
                    @Override
                    public void onFailure(Call<ContactModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }

}
