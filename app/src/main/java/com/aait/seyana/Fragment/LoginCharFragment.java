package com.aait.seyana.Fragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.seyana.Activities.LoginActivity;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Constant;
import com.aait.seyana.Utils.Util;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Aya on 10/11/2017.
 */

public class LoginCharFragment extends BaseFragment {
    @BindView(R.id.iv_provider)
    ImageView iv_provider;
    @BindView(R.id.iv_benfiter)
    ImageView iv_benfiter;
    @BindView(R.id.tv_visitor)
    TextView tv_visitor;
    String character="";
    @Override
    protected int getViewId() {
        return  R.layout.fragment_character_login;
    }

    @Override
    protected void init(View view) {
        tv_visitor.setVisibility(View.GONE);


    }

    @Override
    protected String SetTitle() {
        return getString(R.string.login_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
        return new LoginActivity();
    }

    @Override
    public void onClick(View view) {

    }
    @OnClick(R.id.iv_provider)
    void Provider()
    {
        globalPreferences.storeLoginAs(Constant.provider);
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(LoginFragment.class.getName()).replace(R.id.content, new LoginFragment()).commit();

    }
    @OnClick(R.id.iv_benfiter)
    void Benfiter()
    {
        globalPreferences.storeLoginAs(Constant.benfiter);
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(LoginFragment.class.getName()).replace(R.id.content, new LoginFragment()).commit();

    }

}
