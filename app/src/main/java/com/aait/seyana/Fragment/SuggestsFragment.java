package com.aait.seyana.Fragment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.aait.seyana.Activities.LoginActivity;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Models.Login.LoginData;
import com.aait.seyana.Models.Login.LoginModel;
import com.aait.seyana.Models.Suggests.SuggestsModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aya on 10/11/2017.
 */

public class SuggestsFragment extends BaseFragment {
    @BindView(R.id.btn_send)
    Button btn_send;
    @BindView(R.id.ed_name)
    EditText ed_name;
    @BindView(R.id.ed_phone)
    EditText ed_phone;
    @BindView(R.id.ed_email)
    EditText ed_email;
    @BindView(R.id.ed_message)
    EditText ed_message;

    @Override
    protected int getViewId() {
        return  R.layout.fragment_suggestes;
    }

    @Override
    protected void init(View view) {

    }

    @Override
    protected String SetTitle() {
        return getString(R.string.suggests_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
            return new MainActivity();

    }

    @Override
    public void onClick(View view) {

    }
    @OnClick(R.id.btn_send)
    void Send()
    {
        if (validationSuggests())
        {
            getSuggests();
        }
    }

    private boolean validationSuggests() {
        if (!validateName()) {
            return false;
        }
        if (!validatePhone()) {
            return false;
        }
        if (!validateEmail()) {
            return false;
        }
        if (!validateMessage()) {
            return false;
        }
        return true;
    }
    private boolean validateName() {
        if (ed_name.getText().toString().trim().isEmpty()) {
            ed_name.setError(getString(R.string.please_enter_name));
            Util.requestFocus(ed_name, getActivity().getWindow());
            return false;
        }
        return true;
    }
    private boolean validatePhone() {
        if (ed_phone.getText().toString().trim().isEmpty()) {
            ed_phone.setError(getString(R.string.please_enter_phone));
            Util.requestFocus(ed_phone, getActivity().getWindow());
            return false;
        }
        else if (ed_phone.getText().toString().length()<10)
        {
            ed_phone.setError(getString(R.string.enter_phone_ten));
            Util.requestFocus(ed_phone, getActivity().getWindow());
            return false;
        }
        return true;
    }
    private boolean validateEmail() {
        if (ed_email.getText().toString().trim().isEmpty()) {
            ed_email.setError(getString(R.string.please_enter_email));
            Util.requestFocus(ed_email, getActivity().getWindow());
            return false;
        }
        else {
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(ed_email.getText().toString()).matches()) {
                ed_email.setError(getString(R.string.please_enter_correct_email));
                Util.requestFocus(ed_email, getActivity().getWindow());
                return false;
            }
        }
        return true;
    }
    private boolean validateMessage() {
        if (ed_message.getText().toString().trim().isEmpty()) {
            ed_message.setError(getString(R.string.please_enter_message));
            Util.requestFocus(ed_message, getActivity().getWindow());
            return false;
        }
        return true;
    }
    public void getSuggests() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getSuggests(ed_name.getText().toString(),ed_phone.getText().toString(),
                ed_email.getText().toString(),ed_message.getText().toString())
                .enqueue(new Callback<SuggestsModel>() {

                    @Override
                    public void onResponse(Call<SuggestsModel> call, Response<SuggestsModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            toaster.makeToast(getString(R.string.send_done));
                            getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(CongratulationSendFragment.class.getName()).replace(R.id.content, new CongratulationSendFragment()).commit();

                        }
                        else
                        {
                            toaster.makeToast(response.body().getMsg());
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<SuggestsModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }
}
