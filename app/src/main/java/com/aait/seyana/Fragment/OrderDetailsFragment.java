package com.aait.seyana.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Adapter.SliderAdapter;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Models.AcceptOrder.AcceptModel;
import com.aait.seyana.Models.GetOrder.GetOrderModel;
import com.aait.seyana.Models.RefuseModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aya on 10/11/2017.
 */

public class OrderDetailsFragment extends BaseFragment {

    @BindView(R.id.tv_order)
    TextView tv_order;
    @BindView(R.id.btn_accept)
    TextView btn_accept;
    @BindView(R.id.btn_refuse)
    TextView btn_refuse;
    String id="";
    String name="";
    @BindView(R.id.pager)
    ViewPager mPager;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    private static int currentPage = 0;

    @Override
    protected int getViewId() {
        return  R.layout.fragment_order_details;
    }

    @Override
    protected void init(View view) {
        id=getArguments().getString("id");
        name=getArguments().getString("name");
       // toaster.makeToast(name);

        getOrder();

    }



    @Override
    protected String SetTitle() {
        return getString(R.string.user_order_title)+ " "+id;
    }

    @Override
    protected AppCompatActivity ActivityType() {
        return new MainActivity();
    }

    @Override
    public void onClick(View view) {

    }
    @OnClick(R.id.btn_accept)
    void Accept()
    {
        getAccept();

    }
    @OnClick(R.id.btn_refuse)
    void Refuse()
    {
        getRefuse();

    }

    public void getAccept() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAccept(id,globalPreferences.getID())
                .enqueue(new Callback<AcceptModel>() {

                    @Override
                    public void onResponse(Call<AcceptModel> call, Response<AcceptModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            toaster.makeToast(getString(R.string.accept_done));
                                Bundle bundle = new Bundle();
                                bundle.putString("id", id);
                                bundle.putString("name", name);
                                bundle.putString("lat", response.body().getData().getLat());
                                bundle.putString("lng", response.body().getData().getLng());
                                bundle.putString("address", response.body().getData().getAddress());
                                bundle.putString("phone", response.body().getData().getPhone() + "");
                                Map fragment = new Map();
                                fragment.setArguments(bundle);
                                getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(Map.class.getName()).replace(R.id.content, fragment).commit();

                        }
                        else
                        {
                            toaster.makeToast(response.body().getMsg());
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<AcceptModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }
    public void getRefuse() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getRefuse(id,globalPreferences.getID())
                .enqueue(new Callback<RefuseModel>() {

                    @Override
                    public void onResponse(Call<RefuseModel> call, Response<RefuseModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            toaster.makeToast(getString(R.string.refuse_done));
                            getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(NowOrdersFragment.class.getName()).replace(R.id.content, new NowOrdersFragment()).commit();

                        }
                        else
                        {
                            toaster.makeToast(response.body().getKey());
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<RefuseModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }
    public void getOrder() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getOrderDetails(id)
                .enqueue(new Callback<GetOrderModel>() {

                    @Override
                    public void onResponse(Call<GetOrderModel> call, Response<GetOrderModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ) {
                            CustomeProgressDialog.onFinish();
                            tv_order.setText(response.body().getData().getDescription());

                            final String[] XMEN = new String[response.body().getData().getImages().size()];

                            for (int i = 0; i < response.body().getData().getImages().size(); i++)
                                XMEN[i]=response.body().getData().getImages().get(i).getName();

                        final ArrayList<String> XMENArray = new ArrayList<String>();
                            for(int i=0;i<response.body().getData().getImages().size();i++)
                                XMENArray.add(XMEN[i]);

                            mPager.setAdapter(new SliderAdapter(getActivity(),XMENArray));
                            indicator.setViewPager(mPager);

                            // Auto start of viewpager
                            final Handler handler = new Handler();
                            final Runnable Update = new Runnable() {
                                public void run() {
                                    if (currentPage == XMEN.length) {
                                        currentPage = 0;
                                    }
                                    mPager.setCurrentItem(currentPage++, true);
                                }
                            };
                            Timer swipeTimer = new Timer();
                            swipeTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    handler.post(Update);
                                }
                            }, 2500, 2500);
                        }
                        else
                        {
                            toaster.makeToast(response.body().getKey());
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<GetOrderModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }
}
