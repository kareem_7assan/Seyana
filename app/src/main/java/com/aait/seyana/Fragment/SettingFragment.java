package com.aait.seyana.Fragment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Activities.SplashActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Models.ChangeLangModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;
import com.aait.seyana.Widget.Toaster;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aya on 10/11/2017.
 */

public class SettingFragment extends BaseFragment {

    @BindView(R.id.rg_language)
    RadioGroup rg_language;
    @BindView(R.id.rb_arabic)
    RadioButton rb_arabic;
    @BindView(R.id.rb_english)
    RadioButton rb_english;
    @BindView(R.id.rb_ordo)
    RadioButton rb_ordo;
    @BindView(R.id.btn_follow)
    Button btn_follow;
    String state;
    String language;
    int click = 0;
    Toaster toaster;

    @Override
    protected int getViewId() {
        return  R.layout.fragment_settings;
    }

    @Override
    protected void init(View view) {
        language();
        toaster=new Toaster(getActivity());

    }
    @OnClick(R.id.btn_follow)
    void Follow()
    {
        changeLanguage();
    }

    @Override
    protected String SetTitle() {
        return getString(R.string.settings_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
            return new MainActivity();

    }

    @Override
    public void onClick(View view) {

    }
    void language() {
        if (globalPreferences.getLanguage().equals("ar")) {
            rb_arabic.setChecked(true);
        } else if (globalPreferences.getLanguage().equals("en")) {
            rb_english.setChecked(true);
        } else if (globalPreferences.getLanguage().equals("fa")) {
            rb_ordo.setChecked(true);
        }
        rb_arabic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (rb_arabic.isChecked()) {
                    language = "ar";
                    click = 1;

                }
            }
        });
        rb_english.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (rb_english.isChecked()) {
                    language = "en";
                    click = 1;
                }
            }
        });
        rb_ordo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (rb_ordo.isChecked()) {
                    language = "fa";
                    click = 1;
                }
            }
        });
    }
    void changeLanguage() {
        if (language != "" && click == 1) {
            getLanguage();
           // toaster.makeToast(language);
        }
    }

    public void getLanguage() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getLanguage(language,globalPreferences.getID())
                .enqueue(new Callback<ChangeLangModel>() {

                    @Override
                    public void onResponse(Call<ChangeLangModel> call, Response<ChangeLangModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            toaster.makeToast(getString(R.string.sucess_save));
                            globalPreferences.storeLanguage(response.body().getData());
                            Intent mainAct = new Intent(getActivity(), SplashActivity.class);
                            mainAct.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(mainAct);
                            getActivity().finish();
                        }
                        else
                        {
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<ChangeLangModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }
}
