package com.aait.seyana.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aait.seyana.Activities.LoginActivity;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Activities.MapAddressActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Dialogs.ActivateCodeDialog;
import com.aait.seyana.FCM.Config;
import com.aait.seyana.FCM.NotificationUtils;
import com.aait.seyana.Models.Register.RegisterData;
import com.aait.seyana.Models.Register.RegisterModel;
import com.aait.seyana.Models.Service.ServiceData;
import com.aait.seyana.Models.Service.ServiceModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Constant;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aya on 10/11/2017.
 */

public class RegisterFragment extends BaseFragment {
    @BindView(R.id.cv_photo)
    CircleImageView cv_photo;
    @BindView(R.id.ed_name)
    EditText ed_name;
    @BindView(R.id.ed_phone)
    EditText ed_phone;
    @BindView(R.id.ed_address)
    EditText ed_address;
    @BindView(R.id.ed_password)
    EditText ed_password;
    @BindView(R.id.ed_confirm_password)
    EditText ed_confirm_password;
    @BindView(R.id.sp_service)
    Spinner sp_service;
    @BindView(R.id.iv_add_commercial_photo)
    ImageView iv_add_commercial_photo;
    @BindView(R.id.iv_add_identity_photo)
    ImageView iv_add_identity_photo;
    @BindView(R.id.tv_add_commercial_photo)
    TextView tv_add_commercial_photo;
    @BindView(R.id.tv_add_identity_photo)
    TextView tv_add_identity_photo;
    @BindView(R.id.btn_register)
    Button btn_register;
    @BindView(R.id.rl_service)
    RelativeLayout rl_service;
    String city;
    String langitud;
    String latiud;
    String ImageCommiercal ="";
    String ImageIdentity ="";
    String photo ="";
    String code="";
    String id_services = "";
    ArrayList<String> services = new ArrayList<>();
    String[] spinnerItemsForServices;
    int mA=0;
    public String filePath, profileImageFilePath;
    String regId;
    private BroadcastReceiver mRegistrationBroadcastReceiver;


    @Override
    protected int getViewId() {
        return  R.layout.fragment_register;
    }

    @Override
    protected void init(View view) {
        if (globalPreferences.getLoginAs().equals(Constant.benfiter))
        {
            rl_service.setVisibility(View.GONE);
            iv_add_commercial_photo.setVisibility(View.GONE);
            iv_add_identity_photo.setVisibility(View.GONE);
            tv_add_commercial_photo.setVisibility(View.GONE);
            tv_add_identity_photo.setVisibility(View.GONE);
        }
        else {
            getServices();
        }
//        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//
//                // checking for type intent filter
//                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
//                    // gcm successfully registered
//                    // now subscribe to `global` topic to receive app wide notifications
//                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
//                    displayFirebaseRegId();
//                }
//            }
//        };
        displayFirebaseRegId();

    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);
        Util.onPrintLog("Firebase reg id: " + regId);
    }
    @Override
    public void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getActivity().getApplicationContext());
    }
    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
    @OnClick(R.id.ed_address)
    void Address() {
        Intent intent = new Intent(getActivity(), MapAddressActivity.class);
        startActivityForResult(intent, Constant.RequestCode.GETLOCATION);

    }

    @OnClick(R.id.iv_add_commercial_photo)
    void Commercial() {
        mA=1;
        showPictureDialog();
    }
    @OnClick(R.id.iv_add_identity_photo)
    void Identity() {
        mA=2;
        showPictureDialog();
    }
    @OnClick(R.id.cv_photo)
    void Photo() {
        mA=3;
        showPictureDialog();
    }
    @Override
    protected String SetTitle() {
        return getString(R.string.register_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
        if (globalPreferences.getLoginStatus())
            return new MainActivity();
        else
            return new LoginActivity();
    }

    @Override
    public void onClick(View view) {

    }
    private boolean validationProviderRegister() {
        if (!validatePhoto()) {
            return false;
        }
        if (!validateName()) {
            return false;
        }
        if (!validatePhone()) {
            return false;
        }
        if (!validateAddress()) {
            return false;
        }
        if (!validatePassword()) {
            return false;
        } if (!validateConPassword()) {
            return false;
        }
        if (!validateCommercial()) {
            return false;
        } if (!validateIdentity()) {
            return false;
        }
        if (regId == null) {
            Util.makeToast(getActivity(),getString(R.string.error_network));
        }
        return true;
    }
    private boolean validationBenfiterRegister() {
        if (!validatePhoto()) {
            return false;
        }
        if (!validateName()) {
            return false;
        }
        if (!validatePhone()) {
            return false;
        }
        if (!validateAddress()) {
            return false;
        }
        if (!validatePassword()) {
            return false;
        } if (!validateConPassword()) {
            return false;
        }
        if (regId == null) {
            Util.makeToast(getActivity(),getString(R.string.error_network));
        }
        return true;
    }
    private boolean validateName() {
        if (ed_name.getText().toString().trim().isEmpty()) {
            ed_name.setError(getString(R.string.please_enter_name));
            Util.requestFocus(ed_name, getActivity().getWindow());
            return false;
        }
        return true;
    }
    private boolean validatePhone() {
        if (ed_phone.getText().toString().trim().isEmpty()) {
            ed_phone.setError(getString(R.string.please_enter_phone));
            Util.requestFocus(ed_phone, getActivity().getWindow());
            return false;
        }
        else if (ed_phone.getText().toString().length()<10)
        {
            ed_phone.setError(getString(R.string.enter_phone_ten));
            Util.requestFocus(ed_phone, getActivity().getWindow());
            return false;
        }
        return true;
    }
    private boolean validateAddress() {
        if (ed_address.getText().toString().trim().isEmpty()) {
            ed_address.setError(getString(R.string.please_enter_address));
            Util.requestFocus(ed_address, getActivity().getWindow());
            return false;
        }
        return true;
    }
    private boolean validatePassword() {
        if (ed_password.getText().toString().trim().isEmpty()) {
            ed_password.setError(getString(R.string.please_enter_password));
            Util.requestFocus(ed_password, getActivity().getWindow());
            return false;
        }
        return true;
    }
    private boolean validateConPassword() {
        if (ed_confirm_password.getText().toString().trim().isEmpty()) {
            ed_confirm_password.setError(getString(R.string.please_enter_confirm));
            Util.requestFocus(ed_confirm_password, getActivity().getWindow());
            return false;
        }
        else
        {
            if (!ed_password.getText().toString().equals(ed_confirm_password.getText().toString()))
            {
                ed_confirm_password.setError(getString(R.string.please_enter_confirm_correct));
                Util.requestFocus(ed_confirm_password, getActivity().getWindow());
                return false;
            }
        }
        return true;
    }
    public boolean validateIdentity() {
        if (ImageIdentity.equals("")) {
            toaster.makeToast(getString(R.string.please_enter_identity_photo));
            return false;
        }
        return true;

    }
    public boolean validateCommercial() {
        if (ImageCommiercal.equals("")) {
            toaster.makeToast(getString(R.string.please_enter_comerical_photo));
            return false;
        }
        return true;

    }
    public boolean validatePhoto() {
        if (photo.equals("")) {
            toaster.makeToast(getString(R.string.please_enter_photo));
            return false;
        }
        return true;

    }

    public void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle(getResources().getString(
                R.string.text_choosepicture));
        String[] pictureDialogItems = {
                getResources().getString(R.string.text_gallary),
                getResources().getString(R.string.text_camera)};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {

                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constant.PermissionCode.STORAGE);
                                    return;
                                } else {
                                    choosePhotoFromGallaryIntent();
                                }
                                break;
                            case 1:
                                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constant.PermissionCode.CAMERA);
                                    return;
                                } else {
                                    takePhotoFromCameraIntent();
                                }
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
    public void choosePhotoFromGallaryIntent() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, Constant.RequestCode.PHOTO_CHOOSE);
    }

    public void takePhotoFromCameraIntent() {
        Intent cameraIntent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, Constant.RequestCode.Take_PICTURE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.RequestCode.PHOTO_CHOOSE) {
            if (mA==1) {
                handleChoosePhotoFromGallery(data, iv_add_commercial_photo);
            }else if (mA==2) {
                handleChoosePhotoFromGallery(data, iv_add_identity_photo);
            }else if (mA==3) {
                handleChoosePhotoFromGallery(data, cv_photo);
            }
        } else if (requestCode == Constant.RequestCode.Take_PICTURE) {
            if (mA==1) {
                handleTakePhotoFromCamera(data, iv_add_commercial_photo);
            }else if (mA==2) {
                handleTakePhotoFromCamera(data, iv_add_identity_photo);
            }
            else if (mA==3) {
                handleTakePhotoFromCamera(data, cv_photo);
            }
        }
       else if (requestCode == Constant.RequestCode.GETLOCATION) {
            // Log.d("jj", mAdresse + mLat + mLang);

            if (data != null) {
                city= data.getStringExtra(Constant.LOCATION);
                langitud = data.getStringExtra(Constant.LNG);
                latiud = data.getStringExtra(Constant.LAT);
                ed_address.setText(city);
            }

        }
    }
    public void handleChoosePhotoFromGallery(Intent data, ImageView mImageView) {
        if (data != null) {
            Uri uri = data.getData();
            profileImageFilePath = getRealPathFromURI(uri);
            filePath = profileImageFilePath;
            setPic(mImageView, filePath);
        }
    }
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    private void setPic(ImageView mImageView, String currentPath) {
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(currentPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(currentPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
        if (mA==1) {
            ImageCommiercal = Util.getEncoded64ImageStringFromBitmap(bitmap);
        }
        else if (mA==2) {
            ImageIdentity = Util.getEncoded64ImageStringFromBitmap(bitmap);
        }
        else if (mA==3) {
            photo = Util.getEncoded64ImageStringFromBitmap(bitmap);
        }
    }
    private void handleTakePhotoFromCamera(Intent data ,ImageView mImageView) {
        Bundle extras = data.getExtras();
        Bitmap imageBitmap = (Bitmap) extras.get("data");
        Uri tempUri = getImageUri(getActivity(), imageBitmap);
        File finalFile = new File(getRealPathFromURI(tempUri));
        setPic(mImageView, finalFile.getAbsolutePath());
        if (mA==1) {
            ImageCommiercal = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
        }
        else if (mA==2) {
            ImageIdentity = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
        }
        else if (mA==3) {
            photo = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
        }

    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    public void getProviderRegister() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        Log.e("hh",FirebaseInstanceId.getInstance().getToken());
        SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);
        RetroWeb.getClient().create(ServiceApi.class).getRegister(photo,ed_name.getText().toString(),
                ed_phone.getText().toString(),ed_address.getText().toString(),latiud,langitud,
                ed_password.getText().toString(),id_services,ImageCommiercal,ImageIdentity,
                globalPreferences.getLanguage(),
                pref.getString("regId", null))
                .enqueue(new Callback<RegisterModel>() {


            @Override
            public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                CustomeProgressDialog.onFinish();
                Util.onPrintLog(response.body());

                    if (response.body().getValue().equals("1") ){
                        CustomeProgressDialog.onFinish();
                        RegisterData registerData=response.body().getUser();
                        code=registerData.getMsg();
                        ActivateCodeDialog activateCodeDialog = new ActivateCodeDialog();
                        activateCodeDialog.setTargetFragment(RegisterFragment.this, 300);
                        Bundle args = new Bundle();
                        args.putString("code", code);
                        args.putString("id",registerData.getId());
                        args.putString("name",registerData.getName());
                        args.putString("mobile",registerData.getMobile());
                        args.putString("lat",registerData.getLat());
                        args.putString("lng",registerData.getLng());
                        args.putString("address",registerData.getAddress());
                        args.putString("avatar",registerData.getAvatar());
                        args.putString("role",registerData.getRole());
                        args.putString("service_id",registerData.getService_id());

                        Log.e("code",">>>>>>"+code);
                        activateCodeDialog.setArguments(args);
                        activateCodeDialog.show(getActivity().getSupportFragmentManager(), "Dialog");

                    }
                    else
                    {
                        toaster.makeToast(response.body().getMsg());
                        CustomeProgressDialog.onFinish();

                    }
                }


            @Override
            public void onFailure(Call<RegisterModel> call, Throwable t) {
                CustomeProgressDialog.onFinish();
                Util.onPrintLog(t.getMessage());
            }

        });
    }
    public void getBenfiterRegister() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getRegister(photo,ed_name.getText().toString(),
                ed_phone.getText().toString(),ed_address.getText().toString(),latiud,langitud,
                ed_password.getText().toString(),"",ImageCommiercal,ImageIdentity,
                globalPreferences.getLanguage(),
                regId)
                .enqueue(new Callback<RegisterModel>() {


                    @Override
                    public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            RegisterData registerData=response.body().getUser();
                            code=registerData.getMsg();
                            ActivateCodeDialog activateCodeDialog = new ActivateCodeDialog();
                            activateCodeDialog.setTargetFragment(RegisterFragment.this, 300);
                            Bundle args = new Bundle();
                            args.putString("code", code);
                            args.putString("id",registerData.getId());
                            args.putString("name",registerData.getName());
                            args.putString("mobile",registerData.getMobile());
                            args.putString("lat",registerData.getLat());
                            args.putString("lng",registerData.getLng());
                            args.putString("address",registerData.getAddress());
                            args.putString("avatar",registerData.getAvatar());
                            args.putString("role",registerData.getRole());

                            activateCodeDialog.setArguments(args);
                            activateCodeDialog.show(getActivity().getSupportFragmentManager(), "Dialog");

                        }
                        else
                        {
                            toaster.makeToast(response.body().getMsg());
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<RegisterModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }

    @OnClick(R.id.btn_register)
    void Register()
    {
       /* toaster.makeToast(photo);
        toaster.makeToast(ed_name.getText().toString());
        toaster.makeToast(ed_phone.getText().toString());
        toaster.makeToast(ed_address.getText().toString());
        toaster.makeToast(latiud);
        toaster.makeToast(langitud);
        toaster.makeToast(ed_password.getText().toString());
        toaster.makeToast(id_services);
        toaster.makeToast(ImageCommiercal);
        toaster.makeToast(ImageIdentity);
        toaster.makeToast(regId);*/
        if (globalPreferences.getLoginAs().equals(Constant.provider)) {
            if (validationProviderRegister()) {
                getProviderRegister();
               // new ActivateCodeDialog().show(getActivity().getSupportFragmentManager(), "Dialog");


            }
        }
        else  if (globalPreferences.getLoginAs().equals(Constant.benfiter)) {
            if (validationBenfiterRegister()) {
               getBenfiterRegister();
                Log.e("phot",photo);

            }
        }
    }


    public void SetSpinnerServices() {
        ArrayAdapter<CharSequence> category_adapter = new ArrayAdapter<CharSequence>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, spinnerItemsForServices
        );
        sp_service.setAdapter(category_adapter);
    }
    public void getServices() {
        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getService().enqueue(new Callback<ServiceModel>() {
            @Override
            public void onResponse(Call<ServiceModel> call, Response<ServiceModel> response) {
                customeProgressDialog.onFinish();
                final List<ServiceData> arrayList = response.body().getData();
                if (arrayList!=null) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        services.add(arrayList.get(i).getTitle());
                    }
                    spinnerItemsForServices = new String[services.size()];
                    spinnerItemsForServices = services.toArray(spinnerItemsForServices);
                    SetSpinnerServices();
                }
                else {customeProgressDialog.onFinish();}
                sp_service.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        id_services =""+arrayList.get(position).getId();
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {}
                });
            }
            @Override
            public void onFailure(Call<ServiceModel> call, Throwable t) {
                customeProgressDialog.onFinish();}

        });
    }
}
