package com.aait.seyana.Fragment;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Adapter.NowOrdersAdapter;
import com.aait.seyana.Adapter.OrdersAdapter;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Listners.INotificationFragment;
import com.aait.seyana.Models.CompletedOrders.CompletedOrderData;
import com.aait.seyana.Models.CompletedOrders.CompleteedOrderModel;
import com.aait.seyana.Models.CurrentOrder.CurrentOrdersModel;
import com.aait.seyana.Models.OrderModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Widget.CustomeProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aya on 2/7/2017.
 */

public class OrdersFragment extends BaseFragment {

    @BindView(R.id.rv_orders)
    RecyclerView rv_orders;
    OrdersAdapter adapter;
    @BindView(R.id.notloading)
    ImageView notloading;
    @BindView(R.id.no_data)
    TextView no_data;
    private List<CompletedOrderData> models;
    @BindView(R.id.activity_main_swipe_refresh_layout)
    SwipeRefreshLayout swiperefresh;

    @Override
    protected int getViewId() {
        return R.layout.fragment_orders;
    }

    @Override
    protected void init(View view) {
        models = new ArrayList<>();
        adapter = new OrdersAdapter(getContext(),models);
        rv_orders.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv_orders.setAdapter(adapter);
        getOrders();
        onRefresh();
    }

    @Override
    protected String SetTitle() {
        return getString(R.string.orders_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
        return new MainActivity();
    }

    @Override
    public void onClick(View view) {

    }

    public static Fragment newInstance() {
        Fragment frag = new OrdersFragment();
        return frag;
    }

    public void getOrders() {
        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCompletedOrder(globalPreferences.getSERVICE_ID(),globalPreferences.getID()).enqueue(new Callback<CompleteedOrderModel>() {
            @Override
            public void onResponse(Call<CompleteedOrderModel> call, Response<CompleteedOrderModel> response) {
                swiperefresh.setRefreshing(false);
                CustomeProgressDialog.onFinish();
                if (response.body().getValue().equals("1"))
                {
                    adapter = new OrdersAdapter(getContext(), response.body().getData());
                rv_orders.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                rv_orders.setAdapter(adapter);
            }
                else if (response.body().getValue().equals("0"))
                {
                    notloading.setVisibility(View.VISIBLE);
                    no_data.setVisibility(View.VISIBLE);
                    // Picasso.with(getActivity()).load(R.mipmap.no_data).into(notloading);
                    no_data.setText(getString(R.string.no_data));
                }
            }
            @Override
            public void onFailure(Call<CompleteedOrderModel> call, Throwable t) {
                customeProgressDialog.onFinish();
                notloading.setVisibility(View.VISIBLE);
                no_data.setVisibility(View.VISIBLE);
                swiperefresh.setRefreshing(false);
                // Picasso.with(getActivity()).load(R.mipmap.wifi_error).into(notloading);
                no_data.setText(getString(R.string.no_internent));

            }

        });
    }
    private void onRefresh() {
        swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getOrders();
                        notloading.setVisibility(View.GONE);
                        no_data.setVisibility(View.GONE);
                    }
                }
        );
        swiperefresh.setColorSchemeResources(R.color.colorPrimary, R.color.orange, R.color.yellow);

    }
}
