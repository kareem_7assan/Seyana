package com.aait.seyana.Fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.seyana.Activities.LoginActivity;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Dialogs.ForgetPhoneDialog;
import com.aait.seyana.FCM.Config;
import com.aait.seyana.FCM.NotificationUtils;
import com.aait.seyana.Models.Login.LoginData;
import com.aait.seyana.Models.Login.LoginModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Constant;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;
import com.google.firebase.messaging.FirebaseMessaging;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aya on 10/11/2017.
 */

public class LoginFragment extends BaseFragment  {
    @BindView(R.id.btn_enter)
    Button btn_enter;
    @BindView(R.id.tv_forget_password)
    TextView tv_forget_password;
    @BindView(R.id.tv_new_account)
    TextView tv_new_account;
    @BindView(R.id.ed_phone)
    EditText ed_phone;
    @BindView(R.id.ed_password)
    EditText ed_password;
    @BindView(R.id.rl_login)
    RelativeLayout rl_login;
    String regId;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    @Override
    protected int getViewId() {
        return  R.layout.fragment_login;
    }

    @Override
    protected void init(View view) {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();
                }
            }
        };
        displayFirebaseRegId();
    }
    private void displayFirebaseRegId() {
        SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);
        Util.onPrintLog("Firebase reg id: " + regId);
    }
    @Override
    public void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getActivity().getApplicationContext());
    }
    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    public static Fragment newInstance() {
        Fragment frag = new LoginFragment();
        return frag;
    }
    @Override
    protected String SetTitle() {
        return getString(R.string.login_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
        if (globalPreferences.getLoginStatus())
            return new MainActivity();
        else
            return new LoginActivity();


    }

    @Override
    public void onClick(View view) {

    }
    @OnClick(R.id.btn_enter)
    void Enter()
    {
        Util.onPrintLog(regId);
        if (validationLogin())
        {
            getLogin();
        }
    }
    @OnClick(R.id.tv_new_account)
    void New_Account()
    {
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(RegisterCharFragment.class.getName()).replace(R.id.content, new RegisterCharFragment()).commit();

    }
    @OnClick(R.id.tv_forget_password)
    void Forget_Password()
    {
        new ForgetPhoneDialog().show(getActivity().getSupportFragmentManager(), "Dialog");

    }
    private boolean validationLogin() {
        if (!validatePhone()) {
            return false;
        }
        if (!validatePassword()) {
            return false;
        }
        if (regId == null) {
            Util.makeToast(getActivity(),getString(R.string.error_network));
        }
        return true;
    }

    private boolean validatePhone() {
        if (ed_phone.getText().toString().trim().isEmpty()) {
            ed_phone.setError(getString(R.string.please_enter_phone));
            Util.requestFocus(ed_phone, getActivity().getWindow());
            return false;
        }
        return true;
    }

    private boolean validatePassword() {
        if (ed_password.getText().toString().trim().isEmpty()) {
            ed_password.setError(getString(R.string.please_enter_password));
            Util.requestFocus(ed_password, getActivity().getWindow());
            return false;
        }
        return true;
    }
    public void getLogin() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getLogin(ed_phone.getText().toString(),ed_password.getText().toString(),regId)
                .enqueue(new Callback<LoginModel>() {

                    @Override
                    public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            LoginData loginData=response.body().getUser();
                            globalPreferences.storeID(loginData.getId());
                            globalPreferences.storeNAME(loginData.getName());
                            globalPreferences.storePHONE(loginData.getMobile());
                            globalPreferences.storeLAT(loginData.getLat());
                            globalPreferences.storeLNG(loginData.getLng());
                            globalPreferences.storeADDRESS(loginData.getAddress());
                            globalPreferences.storeAVATAR(loginData.getAvatar());
                            globalPreferences.storeLoginAs(loginData.getRole());
                            globalPreferences.storeSERVICE_ID(loginData.getService_id());
                            globalPreferences.storeLoginStatus(true);
                            Intent intent = new Intent(getContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_left);
                        }
                        else
                        {
                            toaster.makeToast(response.body().getMsg());
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<LoginModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }
}
