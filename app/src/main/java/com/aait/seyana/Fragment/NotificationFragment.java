package com.aait.seyana.Fragment;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Adapter.NotificationAdapter;
import com.aait.seyana.Adapter.NowOrdersAdapter;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Models.CurrentOrder.CurrentOrdersModel;
import com.aait.seyana.Models.Notification.NotificationData;
import com.aait.seyana.Models.Notification.NotificationModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aya on 2/7/2017.
 */

public class NotificationFragment extends BaseFragment  {

    @BindView(R.id.rv_notifications)
    RecyclerView rv_notification;
    NotificationAdapter adapter;
    @BindView(R.id.notloading)
    ImageView notloading;
    @BindView(R.id.no_data)
    TextView no_data;
    private List<NotificationData> models;
    @BindView(R.id.activity_main_swipe_refresh_layout)
    SwipeRefreshLayout swiperefresh;

    @Override
    protected int getViewId() {
        return R.layout.fragment_notification;
    }

    @Override
    protected void init(View view) {
        //toaster.makeToast(globalPreferences.getID());
        models = new ArrayList<>();
        adapter = new NotificationAdapter(getContext(),models);
        rv_notification.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv_notification.setAdapter(adapter);
        getNotification();
        onRefresh();
    }

    @Override
    protected String SetTitle() {
        return getString(R.string.notification_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
        return new MainActivity();
    }

    @Override
    public void onClick(View view) {

    }

    public static Fragment newInstance() {
        Fragment frag = new NotificationFragment();
        return frag;
    }

    public void getNotification() {
        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getNotification(globalPreferences.getID()).enqueue(new Callback<NotificationModel>() {
            @Override
            public void onResponse(Call<NotificationModel> call, Response<NotificationModel> response) {
                Util.onPrintLog(response.body());
                swiperefresh.setRefreshing(false);
                CustomeProgressDialog.onFinish();
                if (response.body().getValue().equals("1")) {
                    adapter = new NotificationAdapter(getContext(), response.body().getData());
                    rv_notification.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    rv_notification.setAdapter(adapter);
                }
                else {
                        notloading.setVisibility(View.VISIBLE);
                        no_data.setVisibility(View.VISIBLE);
                        // Picasso.with(getActivity()).load(R.mipmap.no_data).into(notloading);
                        no_data.setText(getString(R.string.no_data));
                }
            }
            @Override
            public void onFailure(Call<NotificationModel> call, Throwable t) {
                customeProgressDialog.onFinish();
                notloading.setVisibility(View.VISIBLE);
                no_data.setVisibility(View.VISIBLE);
                swiperefresh.setRefreshing(false);
                // Picasso.with(getActivity()).load(R.mipmap.wifi_error).into(notloading);
                no_data.setText(getString(R.string.no_internent));

            }

        });
    }
    private void onRefresh() {
        swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getNotification();
                        notloading.setVisibility(View.GONE);
                        no_data.setVisibility(View.GONE);
                    }
                }
        );
        swiperefresh.setColorSchemeResources(R.color.colorPrimary, R.color.orange, R.color.yellow);

    }
}
