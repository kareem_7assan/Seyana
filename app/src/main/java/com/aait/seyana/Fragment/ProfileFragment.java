package com.aait.seyana.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Activities.MapAddressActivity;
import com.aait.seyana.Base.BaseFragment;
import com.aait.seyana.Models.Login.LoginData;
import com.aait.seyana.Models.Login.LoginModel;
import com.aait.seyana.Models.Profile.ProfileData;
import com.aait.seyana.Models.Profile.ProfileModel;
import com.aait.seyana.Models.Service.ServiceData;
import com.aait.seyana.Models.Service.ServiceModel;
import com.aait.seyana.Models.UpdateProviderProfile.UpdateProviderData;
import com.aait.seyana.Models.UpdateProviderProfile.UpdateProviderModel;
import com.aait.seyana.Models.UpdateUserProfile.UpdateUserData;
import com.aait.seyana.Models.UpdateUserProfile.UpdateUserModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Constant;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Aya on 10/11/2017.
 */

public class ProfileFragment extends BaseFragment {
    @BindView(R.id.cv_photo)
    CircleImageView cv_photo;
    @BindView(R.id.tv_edit_photo)
    TextView tv_edit_photo;
    @BindView(R.id.ed_name)
    EditText ed_name;
    @BindView(R.id.tv_edit_name)
    TextView tv_edit_name;
    @BindView(R.id.ed_phone)
    EditText ed_phone;
    @BindView(R.id.ed_address)
    EditText ed_address;
    @BindView(R.id.ed_password)
    EditText ed_password;
    @BindView(R.id.tv_edit_password)
    TextView tv_edit_password;
    @BindView(R.id.tv_edit_adresse)
    TextView tv_edit_adresse;
    @BindView(R.id.sp_service)
    Spinner sp_service;
    @BindView(R.id.rl_service)
    RelativeLayout rl_service;
    @BindView(R.id.iv_photo1)
    ImageView iv_photo1;
    @BindView(R.id.iv_photo2)
    ImageView iv_photo2;
    @BindView(R.id.btn_save)
    Button btn_save;
    String id_services = "";
    ArrayList<String> services = new ArrayList<>();
    String[] spinnerItemsForServices;
    int position;
    int mA=0;
    String city;
    String langitud;
    String latiud;
    public String filePath, profileImageFilePath;
    String ImageCommiercal ="";
    String ImageIdentity ="";
    String photo ="";
    @Override
    protected int getViewId() {
        return  R.layout.fragment_profile;
    }

    @Override
    protected void init(View view) {
        if (globalPreferences.getLoginAs().equals(Constant.benfiter))
        {
            rl_service.setVisibility(View.GONE);
            iv_photo1.setVisibility(View.GONE);
            iv_photo2.setVisibility(View.GONE);
            ed_address.setVisibility(View.GONE);
            tv_edit_adresse.setVisibility(View.GONE);
        }
        getProfile();

    }
    @OnClick(R.id.iv_photo1)
    void Commercial() {
        btn_save.setVisibility(View.VISIBLE);
        mA=1;
        showPictureDialog();
    }
    @OnClick(R.id.iv_photo2)
    void Identity() {
        btn_save.setVisibility(View.VISIBLE);
        mA=2;
        showPictureDialog();
    }
    @OnClick(R.id.tv_edit_adresse)
    void Address() {
        Intent intent = new Intent(getActivity(), MapAddressActivity.class);
        startActivityForResult(intent, Constant.RequestCode.GETLOCATION);

    }
    public void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle(getResources().getString(
                R.string.text_choosepicture));
        String[] pictureDialogItems = {
                getResources().getString(R.string.text_gallary),
                getResources().getString(R.string.text_camera)};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {

                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constant.PermissionCode.STORAGE);
                                    return;
                                } else {
                                    choosePhotoFromGallaryIntent();
                                }
                                break;
                            case 1:
                                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constant.PermissionCode.CAMERA);
                                    return;
                                } else {
                                    takePhotoFromCameraIntent();
                                }
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
    public void choosePhotoFromGallaryIntent() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, Constant.RequestCode.PHOTO_CHOOSE);
    }

    public void takePhotoFromCameraIntent() {
        Intent cameraIntent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, Constant.RequestCode.Take_PICTURE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.RequestCode.PHOTO_CHOOSE) {
            if (mA==1) {
                handleChoosePhotoFromGallery(data, iv_photo1);
            }else if (mA==2) {
                handleChoosePhotoFromGallery(data, iv_photo2);
            }else if (mA==3) {
                handleChoosePhotoFromGallery(data, cv_photo);
            }
        } else if (requestCode == Constant.RequestCode.Take_PICTURE) {
            if (mA==1) {
                handleTakePhotoFromCamera(data, iv_photo1);
            }else if (mA==2) {
                handleTakePhotoFromCamera(data, iv_photo2);
            }
            else if (mA==3) {
                handleTakePhotoFromCamera(data, cv_photo);
            }

        } else if (requestCode == Constant.RequestCode.GETLOCATION) {
            // Log.d("jj", mAdresse + mLat + mLang);

            if (data != null) {
                city= data.getStringExtra(Constant.LOCATION);
                langitud = data.getStringExtra(Constant.LNG);
                latiud = data.getStringExtra(Constant.LAT);
                ed_address.setText(city);
            }

        }
    }
    public void handleChoosePhotoFromGallery(Intent data, ImageView mImageView) {
        if (data != null) {
            Uri uri = data.getData();
            profileImageFilePath = getRealPathFromURI(uri);
            filePath = profileImageFilePath;
            setPic(mImageView, filePath);
        }
    }
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    private void setPic(ImageView mImageView, String currentPath) {
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(currentPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(currentPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
        if (mA==1) {
            ImageCommiercal = Util.getEncoded64ImageStringFromBitmap(bitmap);
        }
        else if (mA==2) {
            ImageIdentity = Util.getEncoded64ImageStringFromBitmap(bitmap);
        }
        else if (mA==3) {
            photo = Util.getEncoded64ImageStringFromBitmap(bitmap);
        }
    }
    private void handleTakePhotoFromCamera(Intent data ,ImageView mImageView) {
        Bundle extras = data.getExtras();
        Bitmap imageBitmap = (Bitmap) extras.get("data");
        Uri tempUri = getImageUri(getActivity(), imageBitmap);
        File finalFile = new File(getRealPathFromURI(tempUri));
        setPic(mImageView, finalFile.getAbsolutePath());
        if (mA==1) {
            ImageCommiercal = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
        }
        else if (mA==2) {
            ImageIdentity = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
        }
        else if (mA==3) {
            photo = Util.getEncoded64ImageStringFromBitmap(imageBitmap);
        }

    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    @Override
    protected String SetTitle() {
        return getString(R.string.profile_title);
    }

    @Override
    protected AppCompatActivity ActivityType() {
        return new MainActivity();
    }

    @Override
    public void onClick(View view) {

    }
    @OnClick(R.id.btn_save)
    void Save()
    {
        if (globalPreferences.getLoginAs().equals(Constant.provider))
        {
        if (validationProviderProfile())
        {
            getUpdateProviderProfile();
            btn_save.setVisibility(View.GONE);
        }}
        else  if (globalPreferences.getLoginAs().equals(Constant.benfiter))
        {
            if (validationBenfiterProfile())
            {
                getUpdateUserProfile();
                btn_save.setVisibility(View.GONE);
            }}

    }
    @OnClick(R.id.tv_edit_name)
    void Name()
    {
        ed_name.setEnabled(true);
        btn_save.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.tv_edit_photo)
    void tv_Photo()
    {
        btn_save.setVisibility(View.VISIBLE);
        mA=3;
        showPictureDialog();
    }
    @OnClick(R.id.tv_edit_password)
    void Password()
    {
        MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(ChangePasswordFragment.class.getName()).replace(R.id.content, new ChangePasswordFragment()).commit();

    }

    private boolean validationProviderProfile() {

        if (!validateName()) {
            return false;
        }
        if (!validatePhone()) {
            return false;
        }

        return true;
    }
    private boolean validationBenfiterProfile() {
        if (!validateName()) {
            return false;
        }
        if (!validatePhone()) {
            return false;
        }

        return true;
    }

    public boolean validatePhoto() {
        if (photo.equals("")) {
            toaster.makeToast(getString(R.string.please_enter_photo));
            return false;
        }
        return true;

    }
    private boolean validateName() {
        if (ed_name.getText().toString().trim().isEmpty()) {
            ed_name.setError(getString(R.string.please_enter_name));
            Util.requestFocus(ed_name, getActivity().getWindow());
            return false;
        }
        return true;
    }
    private boolean validatePhone() {
        if (ed_phone.getText().toString().trim().isEmpty()) {
            ed_phone.setError(getString(R.string.please_enter_phone));
            Util.requestFocus(ed_phone, getActivity().getWindow());
            return false;
        }
        return true;
    }

    public void SetSpinnerServices() {
        ArrayAdapter<CharSequence> category_adapter = new ArrayAdapter<CharSequence>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, spinnerItemsForServices
        );
        sp_service.setAdapter(category_adapter);
        sp_service.setSelection(position);
    }
    public void getServices() {
        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getService().enqueue(new Callback<ServiceModel>() {
            @Override
            public void onResponse(Call<ServiceModel> call, Response<ServiceModel> response) {
                customeProgressDialog.onFinish();
                final List<ServiceData> arrayList = response.body().getData();
                if (arrayList!=null) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        services.add(response.body().getData().get(i).getTitle());
                        if (arrayList.get(i).getId() == position) {
                            position = i;
                        }
                    }
                    spinnerItemsForServices = new String[services.size()];
                    spinnerItemsForServices = services.toArray(spinnerItemsForServices);
                    SetSpinnerServices();
                }
                else {customeProgressDialog.onFinish();}
                sp_service.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        btn_save.setVisibility(View.VISIBLE);

                        return false;
                    }
                });
                sp_service.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        id_services =""+arrayList.get(position).getId();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {}
                });
            }
            @Override
            public void onFailure(Call<ServiceModel> call, Throwable t) {
                customeProgressDialog.onFinish();}

        });
    }
    public void getProfile() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProfile(globalPreferences.getID())
                .enqueue(new Callback<ProfileModel>() {

                    @Override
                    public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ) {
                            CustomeProgressDialog.onFinish();

                           // toaster.makeToast(response.body().getKey());
                            ProfileData loginData=response.body().getUser_data();
                            ed_name.setText(loginData.getName());
                            ed_phone.setText(loginData.getMobile());
                            Picasso.with(getActivity()).load(loginData.getAvatar()).into(cv_photo);

                            if (globalPreferences.getLoginAs().equals(Constant.provider))
                            {
                                position=loginData.getService_id();
                                getServices();
                                ed_address.setText(globalPreferences.getADDRESS());
                                ed_address.setMaxLines(1);
                                Picasso.with(getActivity()).load(loginData.getPermit()).into(iv_photo1);
                                Picasso.with(getActivity()).load(loginData.getIdentity()).into(iv_photo2);
                            }




                        }
                        else
                        {
                            toaster.makeToast(response.body().getKey());
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<ProfileModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }


    public void getUpdateUserProfile() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getUserUpdate(photo,ed_name.getText().toString(),globalPreferences.getID(),
                latiud,langitud,city)
                .enqueue(new Callback<UpdateUserModel>() {

                    @Override
                    public void onResponse(Call<UpdateUserModel> call, Response<UpdateUserModel> response) {
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            toaster.makeToast(getString(R.string.sucess_save));
                            UpdateUserData loginData=response.body().getUser_data();
                            globalPreferences.storeNAME(loginData.getName());
                            globalPreferences.storeAVATAR(loginData.getAvatar());

                        }
                        else
                        {
                           // toaster.makeToast(response.body().getKey());
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<UpdateUserModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }

    public void getUpdateProviderProfile() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProviderUpdate(photo,ed_name.getText().toString(),globalPreferences.getID(),id_services,ImageCommiercal,ImageIdentity)
                .enqueue(new Callback<UpdateProviderModel>() {

                    @Override
                    public void onResponse(Call<UpdateProviderModel> call, Response<UpdateProviderModel> response) {
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            toaster.makeToast(getString(R.string.sucess_save));
                            UpdateProviderData loginData=response.body().getUser_data();
                            globalPreferences.storeNAME(loginData.getName());
                            globalPreferences.storeAVATAR(loginData.getAvatar());
                            globalPreferences.storeSERVICE_ID(id_services);
                            globalPreferences.storeADDRESS(city);

                        }
                        else
                        {
                            // toaster.makeToast(response.body().getKey());
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<UpdateProviderModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }
}
