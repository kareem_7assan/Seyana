package com.aait.seyana.Base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aait.seyana.Activities.LoginActivity;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Utils.GlobalPreferences;
import com.aait.seyana.Widget.CustomeProgressDialog;
import com.aait.seyana.Widget.Toaster;

import butterknife.ButterKnife;


public abstract class BaseFragment extends Fragment implements View.OnClickListener {

    public GlobalPreferences globalPreferences;
    public Bundle savedInstanceState;
    public Toaster toaster;
    protected CustomeProgressDialog customeProgressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getViewId(), container, false);
        globalPreferences = new GlobalPreferences(getActivity());
        this.savedInstanceState = savedInstanceState;
        toaster = new Toaster(getActivity());
        customeProgressDialog = new CustomeProgressDialog(getActivity(), "");
        ButterKnife.bind(this, view);
        init(view);

//        if (IsLogo()) {
//            MainActivity.tv_image.setVisibility(View.VISIBLE);
//        } else {
//            MainActivity.tv_image.setVisibility(View.GONE);
//        }

//        if (SetTitle() != null) {
//            if (ActivityType().getClass().equals(MainActivity.class)) {
//                Log.e("hgdvshg", ">>>" + SetTitle());
//                MainActivity.tv_title.setText(SetTitle());
//            } else if (ActivityType().getClass().equals(LoginActivity.class)) {
//                Log.e("hgdvshg", ">>>" + SetTitle());
//                if ( MainActivity.tv_title)
//                LoginActivity.tv_title.setText(SetTitle());
//
//            }
//        } else {
//            Log.e("hgdvshg",">>>"+"mnfdbjlndfbjndfln");
//
//        }
        if (SetTitle() != null) {
            if (MainActivity.tv_title!=null) {
                MainActivity.tv_title.setText(SetTitle());

            } if (LoginActivity.tv_title!=null) {
                LoginActivity.tv_title.setText(SetTitle());
            }

        } else {
            Log.e("hgdvshg",">>>"+"mnfdbjlndfbjndfln");

        }
        return view;
    }


    protected abstract int getViewId();

    protected abstract void init(View view);

    protected abstract String SetTitle();

    protected abstract AppCompatActivity ActivityType();

    // protected abstract boolean IsLogo();

}
