package com.aait.seyana.Dialogs;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.aait.seyana.Models.ForgetPhoneModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;
import com.aait.seyana.Widget.Toaster;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPhoneDialog extends BottomSheetDialogFragment {
    EditText ed_phone;
    Button btn_send;
    LinearLayout sheet;
    Toaster toaster;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.phone_dialog_bottom_sheet, container, false);
        sheet=(LinearLayout) v.findViewById(R.id.bottomSheetLayout);
        toaster=new Toaster(getActivity());

        ed_phone=(EditText)v.findViewById(R.id.ed_phone);
        btn_send=(Button) v.findViewById(R.id.btn_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view==btn_send) {
                    if (validationPhone()) {
                        getForgetPhone();
                    }
                }
            }
        });

        return v;
    }
    private boolean validationPhone() {
        if (!validatePhone()) {
            return false;
        }
        return true;
    }

    private boolean validatePhone() {
        if (ed_phone.getText().toString().trim().isEmpty()) {
            ed_phone.setError(getString(R.string.please_enter_phone));
            Util.requestFocus(ed_phone, getActivity().getWindow());
            return false;
        }
        return true;
    }
    public void getForgetPhone() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getForget(ed_phone.getText().toString())
                .enqueue(new Callback<ForgetPhoneModel>() {

                    @Override
                    public void onResponse(Call<ForgetPhoneModel> call, Response<ForgetPhoneModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            toaster.makeToast(response.body().getMsg());
                            Log.e("password",response.body().getPassword()+"");
                            dismiss();


                        }
                        else
                        {
                            toaster.makeToast(response.body().getMsg());
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<ForgetPhoneModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }

}

