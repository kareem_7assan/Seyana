package com.aait.seyana.Dialogs;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Fragment.CongratulationCompleteFragment;
import com.aait.seyana.Fragment.OrdersFragment;
import com.aait.seyana.Models.ActiveUserOrderModel;
import com.aait.seyana.Models.ActiveVisitorOrder.ActiveVisitorOrderModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;
import com.aait.seyana.Widget.Toaster;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BenfiterCodeDialog extends BottomSheetDialogFragment {
    EditText ed_code;
    Button btn_end;
    String id="";
    Toaster toaster;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.benfiter_code_dialog_bottom_sheet, container, false);
        ed_code=(EditText)v.findViewById(R.id.ed_code);
        btn_end=(Button) v.findViewById(R.id.btn_end);
        id=getArguments().getString("id");
        toaster=new Toaster(getActivity());
        btn_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view==btn_end) {
                    if (validationCode()) {
                       getConfirm();
                    }
                }
            }
        });
        return v;
    }
    private boolean validationCode() {
        if (!validatePhone()) {
            return false;
        }
        return true;
    }

    private boolean validatePhone() {
        if (ed_code.getText().toString().trim().isEmpty()) {
            ed_code.setError(getString(R.string.enter_code));
            Util.requestFocus(ed_code, getActivity().getWindow());
            return false;
        }
        return true;
    }

    public void getConfirm() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getActiveUser(id,ed_code.getText().toString())
                .enqueue(new Callback<ActiveUserOrderModel>() {

                    @Override
                    public void onResponse(Call<ActiveUserOrderModel> call, Response<ActiveUserOrderModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            toaster.makeToast(getString(R.string.finish_order_done));
                            dismiss();
                            MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(OrdersFragment.class.getName()).replace(R.id.content, new OrdersFragment()).commit();


                        }
                        else
                        {
                            toaster.makeToast(response.body().getMsg());
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<ActiveUserOrderModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }

}

