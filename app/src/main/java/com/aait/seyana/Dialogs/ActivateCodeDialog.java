package com.aait.seyana.Dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Fragment.CongratulationCompleteFragment;
import com.aait.seyana.Models.ActiveCodeModel;
import com.aait.seyana.Models.ActiveVisitorOrder.ActiveVisitorOrderModel;
import com.aait.seyana.Network.RetroWeb;
import com.aait.seyana.Network.ServiceApi;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Constant;
import com.aait.seyana.Utils.GlobalPreferences;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.CustomeProgressDialog;
import com.aait.seyana.Widget.Toaster;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ActivateCodeDialog extends BottomSheetDialogFragment implements View.OnClickListener {
    LinearLayout sheet;

    EditText ed_phone;
    Button btn_register;
    GlobalPreferences globalPreferences;
    String code,id,name,mobile,lat,lng,address,avatar,role,order_id,service_id;
    Toaster toaster;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activate_code_dialog_bottom_sheet, container, false);
        sheet=(LinearLayout) v.findViewById(R.id.bottomSheetLayout);
        ed_phone=(EditText)v.findViewById(R.id.ed_phone);
        btn_register=(Button) v.findViewById(R.id.btn_register);
        toaster=new Toaster(getActivity());
        globalPreferences=new GlobalPreferences(getActivity());

        if (globalPreferences.getLoginAs().equals(Constant.provider) || globalPreferences.getLoginAs().equals(Constant.benfiter)) {
            code = getArguments().getString("code");
            id = getArguments().getString("id");
            name = getArguments().getString("name");
            mobile = getArguments().getString("mobile");
            lat = getArguments().getString("lat");
            lng = getArguments().getString("lng");
            address = getArguments().getString("address");
            avatar = getArguments().getString("avatar");
            role = getArguments().getString("role");
            service_id = getArguments().getString("service_id");

        }
        else
        {
            btn_register.setText(getString(R.string.end));
            order_id=getArguments().getString("id");
        }
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view==btn_register) {
                    if (validationRegister()) {
                            //  sheet.getBottom();
                            if (globalPreferences.getLoginAs().equals(Constant.provider) || globalPreferences.getLoginAs().equals(Constant.benfiter)) {
                                if (ed_phone.getText().toString().equals(code)) {
                                   getActive();
                                }
                            } else if (globalPreferences.getLoginAs().equals(Constant.visitor)) {
                                 getActiveVisitor();
                            }

                    }
                }
            }
        });

        return v;
    }
    private boolean validationRegister() {
        if (!validatePhone()) {
            return false;
        }
        return true;
    }

    private boolean validatePhone() {
        if (ed_phone.getText().toString().trim().isEmpty()) {
            ed_phone.setError(getString(R.string.enter_code));
            Util.requestFocus(ed_phone, getActivity().getWindow());
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {


    }
    public void getActive() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getActive(id,ed_phone.getText().toString())
                .enqueue(new Callback<ActiveCodeModel>() {

                    @Override
                    public void onResponse(Call<ActiveCodeModel> call, Response<ActiveCodeModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            toaster.makeToast(response.body().getKey());
                            globalPreferences.storeID(id);
                            globalPreferences.storeNAME(name);
                            globalPreferences.storePHONE(mobile);
                            globalPreferences.storeLAT(lat);
                            globalPreferences.storeLNG(lng);
                            globalPreferences.storeADDRESS(address);
                            globalPreferences.storeAVATAR(avatar);
                            globalPreferences.storeLoginAs(role);
                            globalPreferences.storeSERVICE_ID(service_id);
                            globalPreferences.storeLoginStatus(true);
                            Intent intent = new Intent(getContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_left);

                        }
                        else
                        {
                            toaster.makeToast(response.body().getKey());
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<ActiveCodeModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }
    public void getActiveVisitor() {

        CustomeProgressDialog.onStart(getActivity(), getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getActiveVisitor(order_id,ed_phone.getText().toString())
                .enqueue(new Callback<ActiveVisitorOrderModel>() {

                    @Override
                    public void onResponse(Call<ActiveVisitorOrderModel> call, Response<ActiveVisitorOrderModel> response) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(response.body());

                        if (response.body().getValue().equals("1") ){
                            CustomeProgressDialog.onFinish();
                            toaster.makeToast(getString(R.string.active_order_done));
                            dismiss();
                            getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(CongratulationCompleteFragment.class.getName()).replace(R.id.content, new CongratulationCompleteFragment()).commit();


                        }
                        else
                        {
                            toaster.makeToast(response.body().getMsg());
                            CustomeProgressDialog.onFinish();

                        }
                    }


                    @Override
                    public void onFailure(Call<ActiveVisitorOrderModel> call, Throwable t) {
                        CustomeProgressDialog.onFinish();
                        Util.onPrintLog(t.getMessage());
                    }

                });
    }
}

