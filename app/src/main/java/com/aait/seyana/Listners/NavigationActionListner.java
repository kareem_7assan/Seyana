package com.aait.seyana.Listners;

/**
 * Created by mahmoud on 12/03/2017.
 */

public interface NavigationActionListner {

    void onMain();
    void onService();
    void onCompletedOrder();
    void onMyOrders();
    void onProfile();
    void onAbout();
    void onSettings();
    void onShare();
    void onContact();
    void onTerms();
    void onSuggests();
    void onLogout();
    void onLogin();
}
