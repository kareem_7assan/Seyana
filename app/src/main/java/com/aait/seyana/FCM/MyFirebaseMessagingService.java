package com.aait.seyana.FCM;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.R;
import com.aait.seyana.Utils.GlobalPreferences;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by Amrel on 09/04/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {




        private GlobalPreferences getLoginStatus;



        @Override
        public void onMessageReceived(RemoteMessage remoteMessage) {
            Log.d("fcm"," there is notofication");
            getLoginStatus = new GlobalPreferences(this);
            onSetNotification(remoteMessage);
        }

      public void onSetNotification(RemoteMessage remoteMessage){
          NotificationCompat.Builder mBuilder =
                  new NotificationCompat.Builder(this)
                          .setSmallIcon(R.mipmap.logo_splash)
                          .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                          .setContentTitle(remoteMessage.getData().get("title"))
                          .setContentText(remoteMessage.getData().get("content"))
                          .setOngoing(true);
          Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
          notificationIntent.putExtra("state",1);
          notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
          PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                 notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


          mBuilder.setContentIntent(contentIntent);
          // Gets an instance of the NotificationManager service
          NotificationManager mNotifyMgr =
                  (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

          // Builds the notification and issues it.
          mNotifyMgr.notify(1, mBuilder.build());
      }

    }



