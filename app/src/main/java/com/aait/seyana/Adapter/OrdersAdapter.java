package com.aait.seyana.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.seyana.Models.CompletedOrders.CompletedOrderData;
import com.aait.seyana.R;
import com.aait.seyana.Utils.GlobalPreferences;
import com.aait.seyana.Utils.Util;
import com.aait.seyana.Widget.Toaster;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by aya on 2/7/2017.
 */
public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {
    public static String TAG = OrdersAdapter.class.getSimpleName();

    public static Context context;
    List<CompletedOrderData> Models;
    private int lastPosition = -1;
    Toaster toaster;
    GlobalPreferences globalPreferences;


    public OrdersAdapter(Context context, List<CompletedOrderData> Models) {
        this.context = context;
        this.Models = Models;

    }

    @Override
    public OrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_orders, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final CompletedOrderData model=Models.get(position);

        toaster=new Toaster(context);
        globalPreferences=new GlobalPreferences(context);
        holder.tv_client_order.setText(model.getId()+"");
        holder.tv_service_date.setText(model.getCreated_at());
        holder.tv_service_type.setText(model.getService_name());
        if (model.getUser_name()!=null) {
            Picasso.with(context).load(model.getAvatar()).into(holder.cv_photo);

        }
        else
            Picasso.with(context).load(R.mipmap.rate_us).into(holder.cv_photo);

        holder.iv_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(NewOrdersFragment.class.getName()).replace(R.id.content, new NewOrdersFragment()).commit();
            }
        });
        setAnimation(holder.itemView, position);


    }


    @Override
    public int getItemCount() {
        return Models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_client_order)
        TextView tv_client_order;
        @BindView(R.id.tv_service_date)
        TextView tv_service_date;
        @BindView(R.id.tv_service_type)
        TextView tv_service_type;
        @BindView(R.id.cv_photo)
        CircleImageView cv_photo;
        @BindView(R.id.iv_arrow)
        ImageView iv_arrow;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.up_from_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
            Log.e(TAG, "position: " + position);
        }
    }
}
