package com.aait.seyana.Adapter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.seyana.Activities.LoginActivity;
import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Listners.NavigationActionListner;
import com.aait.seyana.Models.MenuModel;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Constant;
import com.aait.seyana.Utils.GlobalPreferences;
import com.aait.seyana.Widget.Toaster;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by mahmoud on 12/1/2017.
 */
public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.ViewHolder> {

    AppCompatActivity context;
    ArrayList<MenuModel> navigationItem;
    GlobalPreferences globalPreferences;
    NavigationActionListner navigationActionListner;
    Toaster toaster;

    public NavigationDrawerAdapter(AppCompatActivity context, ArrayList<MenuModel> navigationItem, NavigationActionListner navigationActionListner) {
        this.context = context;
        this.navigationItem = navigationItem;
        globalPreferences = new GlobalPreferences(context);
        this.navigationActionListner = navigationActionListner;
    }

    @Override
    public NavigationDrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_menu_row_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NavigationDrawerAdapter.ViewHolder holder, final int position) {
        toaster = new Toaster(context);
        holder.tv_nav_name.setText(navigationItem.get(position).getName());
        Picasso.with(context).load(navigationItem.get(position).getImage_id()).into(holder.iv_nav_pic);


        if (globalPreferences.getLoginStatus()) {
            if (position == 9) {
                holder.iv_line.setVisibility(View.GONE);
            }
            if (globalPreferences.getLoginAs().equals(Constant.provider)) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (position == 0) {
                            navigationActionListner.onMain();
                        }else if (position == 1) {
                            navigationActionListner.onCompletedOrder();
                        } else if (position == 2) {
                            navigationActionListner.onProfile();
                        } else if (position == 3) {
                            navigationActionListner.onAbout();
                        } else if (position == 4) {
                            navigationActionListner.onSettings();
                        } else if (position == 5) {
                            navigationActionListner.onShare();
                        } else if (position == 6) {
                            navigationActionListner.onContact();
                        } else if (position == 7) {
                            navigationActionListner.onTerms();
                        } else if (position == 8) {
                            navigationActionListner.onSuggests();
                        } else if (position == 9) {
                            navigationActionListner.onLogout();
                        }
                        drawerOpenClose();
                    }
                });
            }
           else if (globalPreferences.getLoginAs().equals(Constant.benfiter)) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (position == 0) {
                            navigationActionListner.onService();
                        } else if (position == 1) {
                            navigationActionListner.onMyOrders();
                        }else if (position == 2) {
                            navigationActionListner.onProfile();
                        } else if (position == 3) {
                            navigationActionListner.onAbout();
                        } else if (position == 4) {
                            navigationActionListner.onSettings();
                        } else if (position == 5) {
                            navigationActionListner.onShare();
                        } else if (position == 6) {
                            navigationActionListner.onContact();
                        } else if (position == 7) {
                            navigationActionListner.onTerms();
                        } else if (position == 8) {
                            navigationActionListner.onSuggests();
                        } else if (position == 9) {
                            navigationActionListner.onLogout();
                        }
                        drawerOpenClose();
                    }
                });
            }
        }
        else
        {
            if (position==6)
            {
                holder.iv_line.setVisibility(View.GONE);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (position == 0) {
                        navigationActionListner.onService();
                    } else if (position == 1) {
                        navigationActionListner.onLogin();
                    } else if (position == 2) {
                        navigationActionListner.onAbout();
                    } else if (position == 3) {
                        navigationActionListner.onSettings();
                    } else if (position == 4) {
                        navigationActionListner.onShare();
                    } else if (position == 5) {
                        navigationActionListner.onContact();
                    } else if (position == 6) {
                        navigationActionListner.onTerms();
                    }
                    drawerOpenClose();
                }
            });
        }

    }


    @Override
    public int getItemCount() {
        return navigationItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_nav_name;
        ImageView iv_nav_pic;
        ImageView iv_line;

        TextView tv_number;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_nav_name = (TextView) itemView.findViewById(R.id.tv_nav_name);
            iv_nav_pic = (ImageView) itemView.findViewById(R.id.iv_nav_pic);
            iv_line = (ImageView) itemView.findViewById(R.id.iv_line);

            tv_number = (TextView) itemView.findViewById(R.id.tv_number);

        }
    }


    public void drawerOpenClose() {

            if (globalPreferences.getLanguage().equals("ar") || globalPreferences.getLanguage().equals("fa")) {
                MainActivity.drawerLayout.closeDrawer(Gravity.RIGHT);
            } else {
                MainActivity.drawerLayout.closeDrawer(Gravity.LEFT);
            }


    }
}
