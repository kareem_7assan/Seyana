package com.aait.seyana.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Fragment.AddServiceFragment;
import com.aait.seyana.Fragment.OrderDetailsFragment;
import com.aait.seyana.Listners.INotificationFragment;
import com.aait.seyana.Models.CurrentOrder.CurrentOrderData;
import com.aait.seyana.Models.OrderModel;
import com.aait.seyana.Models.Service.ServiceData;
import com.aait.seyana.R;
import com.aait.seyana.Utils.GlobalPreferences;
import com.aait.seyana.Widget.Toaster;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by aya on 2/7/2017.
 */
public class NowOrdersAdapter extends RecyclerView.Adapter<NowOrdersAdapter.ViewHolder> {
    public static String TAG = NowOrdersAdapter.class.getSimpleName();

    public static Context context;
    List<CurrentOrderData> Models;
    private int lastPosition = -1;
    Toaster toaster;
    GlobalPreferences globalPreferences;


    public NowOrdersAdapter(Context context, List<CurrentOrderData> Models) {
        this.context = context;
        this.Models = Models;

    }

    @Override
    public NowOrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_now_orders, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final CurrentOrderData model=Models.get(position);

        toaster=new Toaster(context);
        globalPreferences=new GlobalPreferences(context);
        setAnimation(holder.itemView, position);
        holder.tv_date.setText(model.getCreated_at());
        if (model.getUser_name()==null) {
            holder.tv_order.setText(context.getResources().getString(R.string.user) +" "+ context.getResources().getString(R.string.order) +" "+ model.getService_name());
            Picasso.with(context).load(R.mipmap.rate_us).into(holder.cv_photo);

        }
        else
        {
            if (globalPreferences.getLanguage().equals("en"))
            {
                holder.tv_order.setText( model.getUser_name() +"  "+context.getResources().getString(R.string.user_nam) + " "+  context.getResources().getString(R.string.order) +" "+  model.getService_name());

            }
            else{
                holder.tv_order.setText(context.getResources().getString(R.string.user_nam) + model.getUser_name() +" "+  context.getResources().getString(R.string.order) +" "+  model.getService_name());

            }

            Picasso.with(context).load(model.getAvatar()).into(holder.cv_photo);

        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
                                                       @Override
                                                       public void onClick(View view) {
                                                           Bundle bundle = new Bundle();
                                                           bundle.putString("id",""+model.getId());
                                                           bundle.putString("name",model.getUser_name());
                                                           OrderDetailsFragment fragment = new OrderDetailsFragment();
                                                           fragment.setArguments(bundle);
         MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(OrderDetailsFragment.class.getName()).replace(R.id.content, fragment).commit();

                                                       }
                                                   });


    }


    @Override
    public int getItemCount() {
        return Models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_date)
        TextView tv_date;
        @BindView(R.id.tv_order)
        TextView tv_order;
        @BindView(R.id.cv_photo)
        CircleImageView cv_photo;
        @BindView(R.id.iv_arrow)
        ImageView iv_arrow;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.up_from_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
            Log.e(TAG, "position: " + position);
        }
    }
}
