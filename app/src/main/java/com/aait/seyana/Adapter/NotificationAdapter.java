package com.aait.seyana.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Fragment.AddServiceFragment;
import com.aait.seyana.Fragment.EndOrderFragment;
import com.aait.seyana.Fragment.MyOrdersFragment;
import com.aait.seyana.Fragment.OrderDetailsFragment;
import com.aait.seyana.Fragment.RatingFragment;
import com.aait.seyana.Listners.INotificationFragment;
import com.aait.seyana.Models.CurrentOrder.CurrentOrderData;
import com.aait.seyana.Models.Notification.NotificationData;
import com.aait.seyana.Models.Notification.NotificationModel;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Constant;
import com.aait.seyana.Utils.GlobalPreferences;
import com.aait.seyana.Widget.Toaster;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;



/**
 * Created by aya on 2/7/2017.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    public static String TAG = NotificationAdapter.class.getSimpleName();

    public static Context context;
    List<NotificationData> Models;
    private int lastPosition = -1;
    Toaster toaster;
    GlobalPreferences globalPreferences;


    public NotificationAdapter(Context context, List<NotificationData> Models) {
        this.context = context;
        this.Models = Models;

    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_notifications, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final NotificationData model=Models.get(position);

        toaster=new Toaster(context);
        globalPreferences=new GlobalPreferences(context);
        holder.tv_date.setText(model.getCreated_at());
        holder.tv_service.setText(model.getContent());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (model.getKey().equals("end"))
                    {
                        Bundle bundle = new Bundle();
                        bundle.putString("id",""+model.getNextId());
                        bundle.putString("name","aya");
                        EndOrderFragment fragment = new EndOrderFragment();
                        fragment.setArguments(bundle);
                        MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(EndOrderFragment.class.getName()).replace(R.id.content, fragment).commit();

                    }
                    else  if (model.getKey().equals("order")&&globalPreferences.getLoginAs().equals(Constant.provider))
                    {
                        Bundle bundle = new Bundle();
                        bundle.putString("id",""+model.getNextId());
                        bundle.putString("name","aya");
                        OrderDetailsFragment fragment = new OrderDetailsFragment();
                        fragment.setArguments(bundle);
                        MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(OrderDetailsFragment.class.getName()).replace(R.id.content, fragment).commit();

                    }
                    else  if (model.getKey().equals("order")&&globalPreferences.getLoginAs().equals(Constant.benfiter))
                    {
                        MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(MyOrdersFragment.class.getName()).replace(R.id.content, new MyOrdersFragment()).commit();

                    }
                    else  if (model.getKey().equals("accept"))
                    {
                    }
                    else  if (model.getKey().equals("service")&&globalPreferences.getLoginAs().equals(Constant.benfiter)) {
                        Bundle bundle = new Bundle();
                        bundle.putString("id", "" + model.getNextId());
                        RatingFragment fragment = new RatingFragment();
                        fragment.setArguments(bundle);
                        MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(RatingFragment.class.getName()).replace(R.id.content, fragment).commit();

                    }

                }
            });

        setAnimation(holder.itemView, position);


    }


    @Override
    public int getItemCount() {
        return Models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_service)
        TextView tv_service;
        @BindView(R.id.tv_date)
        TextView tv_date;
        @BindView(R.id.iv_arrow)
        ImageView iv_arrow;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.up_from_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
            Log.e(TAG, "position: " + position);
        }
    }
}
