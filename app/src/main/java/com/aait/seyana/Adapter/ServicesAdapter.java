package com.aait.seyana.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aait.seyana.Activities.MainActivity;
import com.aait.seyana.Fragment.AddServiceFragment;
import com.aait.seyana.Fragment.ServiceFragment;
import com.aait.seyana.Listners.INotificationFragment;
import com.aait.seyana.Models.OrderModel;
import com.aait.seyana.Models.Service.ServiceData;
import com.aait.seyana.Models.Service.ServiceModel;
import com.aait.seyana.R;
import com.aait.seyana.Utils.GlobalPreferences;
import com.aait.seyana.Widget.Toaster;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by aya on 2/7/2017.
 */
public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {
    public static String TAG = ServicesAdapter.class.getSimpleName();

    public static Context context;
    List<ServiceData> Models;
    private int lastPosition = -1;
    INotificationFragment iNotificationFragment;
    Toaster toaster;
    GlobalPreferences globalPreferences;


    public ServicesAdapter(Context context, List<ServiceData> Models) {
        this.context = context;
        this.Models = Models;

    }

    @Override
    public ServicesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_services, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ServiceData model=Models.get(position);

        toaster=new Toaster(context);
        globalPreferences=new GlobalPreferences(context);
        Picasso.with(context).load(model.getImage()).into(holder.iv_service);
        holder.tv_service.setText(model.getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("id",""+model.getId());
                AddServiceFragment fragment = new AddServiceFragment();
                fragment.setArguments(bundle);
                MainActivity.activity.getSupportFragmentManager().beginTransaction().addToBackStack(AddServiceFragment.class.getName()).replace(R.id.content, fragment).commit();

            }
        });
        setAnimation(holder.itemView, position);


    }


    @Override
    public int getItemCount() {
        return Models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_service)
        ImageView iv_service;
        @BindView(R.id.tv_service)
        TextView tv_service;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.up_from_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
            Log.e(TAG, "position: " + position);
        }
    }
}
