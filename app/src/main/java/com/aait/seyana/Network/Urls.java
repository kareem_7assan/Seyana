package com.aait.seyana.Network;
/*
created by mahmoud 16/4/2017
 */

public class Urls {

    public final static String ENDPOINT = "https://pro3.aait.sa/syana/api/";
    public final static String HTTTPIMG = "https://pro3.aait.sa/syana/";
    public final static String Register = "signup";
    public final static String Active = "active_account";
    public final static String Login = "signin";
    public final static String forgetpassword = "forgetpassword";
    public final static String changepassword = "changepassword";
    public final static String about = "about";
    public final static String conditions = "conditions";
    public final static String contactus = "contactus";
    public final static String suggestion = "suggestion";
    public final static String services = "services";
    public final static String profile = "profile";
    public final static String update_user = "update_user";
    public final static String update_provider = "update_provider";
    public final static String changelanguage = "changelanguage";
    public final static String make_order = "make_order";
    public final static String active_visitor_order = "active_visitor_order";
    public final static String confirm_order = "confirm_order";
    public final static String accept_order = "accept_order";
    public final static String refuse_order = "refuse_order";
    public final static String current_orders = "current_orders";
    public final static String completed_orders = "completed_orders";
    public final static String my_orders = "my_orders";
    public final static String rate = "rate";
    public final static String notifications = "notifications";
    public final static String order = "order";




    public final static String GETLOCATION = "geocode/json?sensor=true";


    /*****  services keys *****/
    public static class Keys {
        public final static String lang = "lang";
        public final static String avatar = "avatar";
        public final static String name = "name";
        public final static String mobile = "mobile";
        public final static String password = "password";
        public final static String address = "address";
        public final static String lat = "lat";
        public final static String lng = "lng";
        public final static String service_id = "service_id";
        public final static String permit = "permit";
        public final static String identity = "identity";
        public final static String device_id = "device_id";
        public final static String user_id = "user_id";
        public final static String msg = "msg";
        public final static String new_password = "new_password";
        public final static String email = "email";
        public final static String content = "content";
        public final static String language = "language";
        public final static String description = "description";
        public final static String images = "images";
        public final static String order_id = "order_id";
        public final static String code = "code";
        public final static String verification_code = "verification_code";
        public final static String rate = "rate";




    }


}
