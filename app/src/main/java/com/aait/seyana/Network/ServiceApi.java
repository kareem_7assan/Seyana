package com.aait.seyana.Network;

import com.aait.seyana.Models.About.AboutModel;
import com.aait.seyana.Models.AcceptOrder.AcceptModel;
import com.aait.seyana.Models.ActiveCodeModel;
import com.aait.seyana.Models.ActiveUserOrderModel;
import com.aait.seyana.Models.ActiveVisitorOrder.ActiveVisitorOrderModel;
import com.aait.seyana.Models.ChangeLangModel;
import com.aait.seyana.Models.ChangePasswordModel;
import com.aait.seyana.Models.CompletedOrders.CompleteedOrderModel;
import com.aait.seyana.Models.Contact.ContactModel;
import com.aait.seyana.Models.CurrentOrder.CurrentOrdersModel;
import com.aait.seyana.Models.ForgetPhoneModel;
import com.aait.seyana.Models.GetOrder.GetOrderModel;
import com.aait.seyana.Models.LocationResponse.LocationResponse;
import com.aait.seyana.Models.Login.LoginModel;
import com.aait.seyana.Models.Notification.NotificationModel;
import com.aait.seyana.Models.Profile.ProfileModel;
import com.aait.seyana.Models.RateModel;
import com.aait.seyana.Models.RefuseModel;
import com.aait.seyana.Models.Register.RegisterModel;
import com.aait.seyana.Models.Service.ServiceModel;
import com.aait.seyana.Models.Suggests.SuggestsModel;
import com.aait.seyana.Models.Terms.TermsModel;
import com.aait.seyana.Models.UpdateProviderProfile.UpdateProviderModel;
import com.aait.seyana.Models.UpdateUserProfile.UpdateUserModel;
import com.aait.seyana.Models.UserMakeOrder.UserMakeOrderModel;
import com.aait.seyana.Models.UserOrder.UserOrdersModel;
import com.aait.seyana.Models.VisitorMakeOrderModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ServiceApi {
    @GET(Urls.GETLOCATION)
    Call<LocationResponse> getLocation(
            @Query("latlng") String lng,
            @Query("language") String languge
    );
    @FormUrlEncoded
    @POST(Urls.Register)
    Call<RegisterModel> getRegister(
            @Field(Urls.Keys.avatar) String avatar,
            @Field(Urls.Keys.name) String name,
            @Field(Urls.Keys.mobile) String mobile,
            @Field(Urls.Keys.address) String address,
            @Field(Urls.Keys.lat) String lat,
            @Field(Urls.Keys.lng) String lng,
            @Field(Urls.Keys.password) String password,
            @Field(Urls.Keys.service_id) String service_id,
            @Field(Urls.Keys.permit) String permit,
            @Field(Urls.Keys.identity) String identity,
            @Field(Urls.Keys.language) String languew,
            @Field(Urls.Keys.device_id) String device_id);

    @FormUrlEncoded
    @POST(Urls.Active)
    Call<ActiveCodeModel> getActive(
            @Field(Urls.Keys.user_id) String user_id,
            @Field(Urls.Keys.msg) String msg);

    @FormUrlEncoded
    @POST(Urls.Login)
    Call<LoginModel> getLogin(
            @Field(Urls.Keys.mobile) String mobile,
            @Field(Urls.Keys.password) String password,
            @Field(Urls.Keys.device_id) String device_id);

    @FormUrlEncoded
    @POST(Urls.forgetpassword)
    Call<ForgetPhoneModel> getForget(
            @Field(Urls.Keys.mobile) String mobile);

    @FormUrlEncoded
    @POST(Urls.changepassword)
    Call<ChangePasswordModel> getChange(
            @Field(Urls.Keys.user_id) String user_id,
            @Field(Urls.Keys.password) String password,
            @Field(Urls.Keys.new_password) String new_password);


    @GET(Urls.about)
    Call<AboutModel> getAbout();

    @GET(Urls.conditions)
    Call<TermsModel> getTerms();

    @GET(Urls.contactus)
    Call<ContactModel> getContact();

    @FormUrlEncoded
    @POST(Urls.suggestion)
    Call<SuggestsModel> getSuggests(
            @Field(Urls.Keys.name) String name,
            @Field(Urls.Keys.mobile) String mobile,
            @Field(Urls.Keys.email) String email,
            @Field(Urls.Keys.content) String content);
    @GET(Urls.services)
    Call<ServiceModel> getService();


    @FormUrlEncoded
    @POST(Urls.profile)
    Call<ProfileModel> getProfile(
            @Field(Urls.Keys.user_id) String user_id);

    @FormUrlEncoded
    @POST(Urls.update_provider)
    Call<UpdateProviderModel> getProviderUpdate(
            @Field(Urls.Keys.avatar) String avatar,
            @Field(Urls.Keys.name) String name,
            @Field(Urls.Keys.user_id) String user_id,
            @Field(Urls.Keys.service_id) String service_id,
            @Field(Urls.Keys.permit) String permit,
            @Field(Urls.Keys.identity) String identity);

    @FormUrlEncoded
    @POST(Urls.update_user)
    Call<UpdateUserModel> getUserUpdate(
            @Field(Urls.Keys.avatar) String avatar,
            @Field(Urls.Keys.name) String name,
            @Field(Urls.Keys.user_id) String user_id,
            @Field(Urls.Keys.lat) String lat,
            @Field(Urls.Keys.lng) String lng,
            @Field(Urls.Keys.address) String addrese);


    @FormUrlEncoded
    @POST(Urls.changelanguage)
    Call<ChangeLangModel> getLanguage(
            @Field(Urls.Keys.language) String language,
            @Field(Urls.Keys.user_id) String user_id);


    @FormUrlEncoded
    @POST(Urls.make_order)
    Call<VisitorMakeOrderModel> getVisitorOrder(
            @Field(Urls.Keys.service_id) String service_id,
            @Field(Urls.Keys.description) String description,
            @Field(Urls.Keys.images) String images,
            @Field(Urls.Keys.address) String address,
            @Field(Urls.Keys.lat) String lat,
            @Field(Urls.Keys.lng) String lng,
            @Field(Urls.Keys.mobile) String mobile);



    @FormUrlEncoded
    @POST(Urls.make_order)
    Call<UserMakeOrderModel> getUserOrder(
            @Field(Urls.Keys.service_id) String service_id,
            @Field(Urls.Keys.description) String description,
            @Field(Urls.Keys.images) String images,
            @Field(Urls.Keys.address) String address,
            @Field(Urls.Keys.lat) String lat,
            @Field(Urls.Keys.lng) String lng,
            @Field(Urls.Keys.user_id) String user_id);



    @FormUrlEncoded
    @POST(Urls.active_visitor_order)
    Call<ActiveVisitorOrderModel> getActiveVisitor(
            @Field(Urls.Keys.order_id) String order_id,
            @Field(Urls.Keys.code) String code);

    @FormUrlEncoded
    @POST(Urls.confirm_order)
    Call<ActiveUserOrderModel> getActiveUser(
            @Field(Urls.Keys.order_id) String order_id,
            @Field(Urls.Keys.verification_code) String verification_code);


    @FormUrlEncoded
    @POST(Urls.accept_order)
    Call<AcceptModel> getAccept(
            @Field(Urls.Keys.order_id) String order_id,
            @Field(Urls.Keys.user_id) String user_id);


    @FormUrlEncoded
    @POST(Urls.refuse_order)
    Call<RefuseModel> getRefuse(
            @Field(Urls.Keys.order_id) String order_id,
            @Field(Urls.Keys.user_id) String user_id);


    @FormUrlEncoded
    @POST(Urls.current_orders)
    Call<CurrentOrdersModel> getCurrentOrder(
            @Field(Urls.Keys.service_id) String service_id,
            @Field(Urls.Keys.user_id) String user_id);

    @FormUrlEncoded
    @POST(Urls.completed_orders)
    Call<CompleteedOrderModel> getCompletedOrder(
            @Field(Urls.Keys.service_id) String service_id,
            @Field(Urls.Keys.user_id) String user_id);


    @FormUrlEncoded
    @POST(Urls.my_orders)
    Call<UserOrdersModel> getMyOrder(
            @Field(Urls.Keys.user_id) String user_id);


    @FormUrlEncoded
    @POST(Urls.rate)
    Call<RateModel> getRate(
            @Field(Urls.Keys.user_id) String user_id,
            @Field(Urls.Keys.service_id) String service_id,
            @Field(Urls.Keys.rate) String rate);

    @FormUrlEncoded
    @POST(Urls.notifications)
    Call<NotificationModel> getNotification(
            @Field(Urls.Keys.user_id) String user_id);

    @FormUrlEncoded
    @POST(Urls.order)
    Call<GetOrderModel> getOrderDetails(
            @Field(Urls.Keys.order_id) String order_id);

    @FormUrlEncoded
    @POST("end_order")
    Call<GetOrderModel> onSendOrderData(
            @Field(Urls.Keys.order_id) String order_id);
}




