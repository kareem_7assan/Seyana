package com.aait.seyana.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Locale;

/*
created by mahmoud 10/4/2017
 */

public class GlobalPreferences {
    final static String APP_lANGUAGE = "language";
    final static String PREFS_NAME = "settings";
    final static String SETTINGS_LOGIN = "login";
    final static String SETTINGS_LoginAs = "loginas";

    final static String SETTINGS_NOTIFICATION = "notification";
    final static String SETTINGS_VIBRATION = "vibration";
    final static String USER = "user";
    final static String SETTINGS_ID = "id";
    final static String SETTINGS_NAME = "name";
    final static String SETTINGS_EMAIL = "email";
    final static String SETTINGS_PHONE= "phone";
    final static String SETTINGS_LAT = "lat";
    final static String SETTINGS_LNG = "lng";
    final static String SETTINGS_ADDRESS = "address";
    final static String SETTINGS_SERVICE_ID = "service_id";
    final static String SETTINGS_CAR_NUM = "car_num";
    final static String SETTINGS_PRICE = "price";
    final static String SETTINGS_SUB_CATEGORY_ID = "sub_category_id";
    final static String SETTINGS_AVATAR= "avatar";
    final static String SETTINGS_IDENTITY_IMAGE = "identity_image";
    final static String SETTINGS_INSURANCE_FILE = "Insurance_file";

    private Context context;
    private SharedPreferences prefs;
    private SharedPreferences.Editor PrefsEditor;


    public GlobalPreferences(Context context) {
        this.context = context;
        prefs = context.getSharedPreferences(PREFS_NAME, 0);
        PrefsEditor = prefs.edit();
    }
    public void storeINSURANCE_FILE(String Insurance_file) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SETTINGS_INSURANCE_FILE, Insurance_file);
        editor.commit();
    }

    public String getINSURANCE_FILE() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        String value = prefs.getString(SETTINGS_INSURANCE_FILE, "");
        return value;
    }
    public void storeIDENTITY_IMAGE(String identity_image) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SETTINGS_IDENTITY_IMAGE, identity_image);
        editor.commit();
    }

    public String getIDENTITY_IMAGE() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        String value = prefs.getString(SETTINGS_IDENTITY_IMAGE, "");
        return value;
    }
    public void storeAVATAR(String avatar) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SETTINGS_AVATAR, avatar);
        editor.commit();
    }

    public String getAVATAR() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        String value = prefs.getString(SETTINGS_AVATAR, "");
        return value;
    }
    public void storeSUB_CATEGORY_ID(String sub_category_id) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SETTINGS_SUB_CATEGORY_ID, sub_category_id);
        editor.commit();
    }

    public String getSUB_CATEGORY_ID() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        String value = prefs.getString(SETTINGS_SUB_CATEGORY_ID, "");
        return value;
    }
    public void storePrice(String price) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SETTINGS_PRICE, price);
        editor.commit();
    }

    public String getPrice() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        String value = prefs.getString(SETTINGS_PRICE, "");
        return value;
    }
    public void storeCAR_NUM(String car_num) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SETTINGS_CAR_NUM, car_num);
        editor.commit();
    }

    public String getCAR_NUM() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        String value = prefs.getString(SETTINGS_CAR_NUM, "");
        return value;
    }
    public void storeSERVICE_ID(String identity_num) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SETTINGS_SERVICE_ID, identity_num);
        editor.commit();
    }

    public String getSERVICE_ID() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        String value = prefs.getString(SETTINGS_SERVICE_ID, "");
        return value;
    }

    public void LogOut() {
        PrefsEditor.clear();
        PrefsEditor.commit();
    }

    public Boolean getLoginStatus() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        Boolean value = prefs.getBoolean(SETTINGS_LOGIN, false);
        return value;
    }

    public void storeLoginStatus(Boolean status) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(SETTINGS_LOGIN, status);
        editor.commit();
    }
    public void storeADDRESS(String address) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SETTINGS_ADDRESS, address);
        editor.commit();
    }

    public String getADDRESS() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        String value = prefs.getString(SETTINGS_ADDRESS, "");
        return value;
    }
    public void storeLNG(String lng) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SETTINGS_LNG, lng);
        editor.commit();
    }

    public String getLNG() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        String value = prefs.getString(SETTINGS_LNG, "");
        return value;
    }
    public void storeLAT(String lat) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SETTINGS_LAT, lat);
        editor.commit();
    }

    public String getLAT() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        String value = prefs.getString(SETTINGS_LAT, "");
        return value;
    }
    public void storePHONE(String phone) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SETTINGS_PHONE, phone);
        editor.commit();
    }

    public String getPHONE() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        String value = prefs.getString(SETTINGS_PHONE, "");
        return value;
    }
    public void storeEMAIL(String email) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SETTINGS_EMAIL, email);
        editor.commit();
    }

    public String getEMAIL() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        String value = prefs.getString(SETTINGS_EMAIL, "");
        return value;
    }
    public void storeNAME(String name) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SETTINGS_NAME, name);
        editor.commit();
    }

    public String getNAME() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        String value = prefs.getString(SETTINGS_NAME, "");
        return value;
    }

    public void storeID(String id) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SETTINGS_ID, id);
        editor.commit();
    }

    public String getID() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        String value = prefs.getString(SETTINGS_ID, "");
        return value;
    }

    public void storeVibration(Boolean status) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(SETTINGS_VIBRATION, status);
        editor.commit();
    }

    public Boolean getVibration() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        Boolean value = prefs.getBoolean(SETTINGS_VIBRATION, false);
        return value;
    }

    public void storeNotification(Boolean status) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(SETTINGS_NOTIFICATION, status);
        editor.commit();
    }

    public Boolean getNotification() {
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        Boolean value = prefs.getBoolean(SETTINGS_NOTIFICATION, true);
        return value;
    }


    public void storeLanguage(String ln) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(APP_lANGUAGE, ln);
        editor.commit();
    }

    public String getLanguage() {
        String value = "";
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        Log.d(APP_lANGUAGE, Locale.getDefault().getDisplayLanguage());
        value = prefs.getString(APP_lANGUAGE, "ar");
        return value;
    }
    public void storeLoginAs(String as) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME,
                0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SETTINGS_LoginAs, as);
        editor.commit();
    }

    public String getLoginAs() {
        String value = "";
        final SharedPreferences prefs = context.getSharedPreferences(
                PREFS_NAME, 0);
        Log.d(SETTINGS_LoginAs, Locale.getDefault().getDisplayLanguage());
        value = prefs.getString(SETTINGS_LoginAs, "provider");
        return value;
    }
}


