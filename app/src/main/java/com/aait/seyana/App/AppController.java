package com.aait.seyana.App;

import android.app.Application;

import com.aait.seyana.Utils.GlobalPreferences;
import com.aait.seyana.Utils.LruBitmapCache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.tsengvn.typekit.Typekit;


/*
created by mahmoud 10/4/2017
 */

public class AppController extends Application {
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;


    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }
    public static final String TAG = AppController.class.getSimpleName();

    private static AppController mInstance;
    public static GlobalPreferences globalPreferences;


    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        globalPreferences = new GlobalPreferences(this);

        if (globalPreferences.getLanguage().equals("fa")) {
            Typekit.getInstance()
                    .addNormal(Typekit.createFromAsset(this, "fonts/" + "Jamil-nory.ttf"))
                    .addBold(Typekit.createFromAsset(this, "fonts/" + "Jamil-nory.ttf"));

        } else {
            Typekit.getInstance()
                    .addNormal(Typekit.createFromAsset(this, "fonts/" + "JF-Flat-regular.ttf"))
                    .addBold(Typekit.createFromAsset(this, "fonts/" + "JF-Flat-medium.ttf"));
        }
    }


    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

}