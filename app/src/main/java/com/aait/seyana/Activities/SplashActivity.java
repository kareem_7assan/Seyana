package com.aait.seyana.Activities;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import com.aait.seyana.Base.ParentActivity;
import com.aait.seyana.R;

public class SplashActivity extends ParentActivity {

    Runnable run;
    Handler handler = new Handler();

    private void initializeUI() {

        Log.e("language", "lang" + globalPreferences.getLanguage());
        run = new Runnable() {
            @Override
            public void run() {
                if (globalPreferences.getLoginStatus()) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_left);
                    finish();
                } else {
                    OpenActivity();
                    finish();
                }
            }
        };
        handler.postDelayed(run, 1000);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected void init() {
        initializeUI();
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputeType() {
        return false;
    }


    public void OpenActivity() {
        Intent intent = new Intent(getApplicationContext(), LanguageActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_left);
        finish();
    }


    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        handler.removeCallbacks(run);
        super.onDestroy();
    }
}
