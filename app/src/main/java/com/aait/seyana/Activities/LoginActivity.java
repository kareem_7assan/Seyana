package com.aait.seyana.Activities;

import android.support.design.widget.AppBarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import com.aait.seyana.Base.ParentActivity;
import com.aait.seyana.Fragment.LoginFragment;
import com.aait.seyana.Fragment.NavigationFragment;
import com.aait.seyana.R;

/**
 * Created by mahmoud on 6/19/2017.
 */

public class LoginActivity extends ParentActivity implements View.OnClickListener {
    public static TextView tv_title;
    public static Toolbar toolbar;
    public static AppBarLayout app_bar;
    public static FragmentManager fragmentManager;
    public static AppCompatActivity activity;
    NavigationFragment navigationFragment;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected void init() {
        initializeUI();

        fragmentManager.beginTransaction().replace(R.id.content, new LoginFragment()).commit();


    }
    private void initializeUI() {
        fragmentManager = getSupportFragmentManager();
        app_bar = (AppBarLayout) findViewById(R.id.app_bar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_title = (TextView) findViewById(R.id.tv_title);
        activity = this;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputeType() {
        return false;
    }

    @Override
    public void onClick(View view) {

    }
}
