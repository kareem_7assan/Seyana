package com.aait.seyana.Activities;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.seyana.Base.ParentActivity;
import com.aait.seyana.Fragment.AboutFragment;
import com.aait.seyana.Fragment.CongratulationCompleteFragment;
import com.aait.seyana.Fragment.CongratulationSendFragment;
import com.aait.seyana.Fragment.ContactFragment;
import com.aait.seyana.Fragment.LoginCharFragment;
import com.aait.seyana.Fragment.LoginFragment;
import com.aait.seyana.Fragment.Map;
import com.aait.seyana.Fragment.MyOrdersFragment;
import com.aait.seyana.Fragment.NavigationFragment;
import com.aait.seyana.Fragment.NotificationFragment;
import com.aait.seyana.Fragment.NowOrdersFragment;
import com.aait.seyana.Fragment.OrdersFragment;
import com.aait.seyana.Fragment.ProfileFragment;
import com.aait.seyana.Fragment.ServiceFragment;
import com.aait.seyana.Fragment.ServicesFragment;
import com.aait.seyana.Fragment.SettingFragment;
import com.aait.seyana.Fragment.SuggestsFragment;
import com.aait.seyana.Fragment.TermsFragment;
import com.aait.seyana.R;
import com.aait.seyana.Utils.Constant;
import com.aait.seyana.Utils.GlobalPreferences;
import com.aait.seyana.Utils.Util;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by aya on 8/10/2017.
 */

public class MainActivity extends ParentActivity implements View.OnClickListener {

    @BindView(R.id.navigate)
    ImageView navigate;
    @BindView(R.id.iv_notification)
    ImageView iv_notification;
    public static TextView tv_title;
    public static ImageView tv_image;
    public static FragmentManager fragmentManager;
    public static AppCompatActivity activity;
    public static DrawerLayout drawerLayout;
    NavigationFragment navigationFragment;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected void init() {
        Util.onPrintLog(globalPreferences.getID());
       // globalPreferences.storeLoginStatus(true);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        fragmentManager = getSupportFragmentManager();
        tv_title = (TextView) findViewById(R.id.tv_title);
        iv_notification.setOnClickListener(this);
        activity = this;
        // fragment manager
        if (globalPreferences.getLoginAs().equals(Constant.provider)) {
            if (getIntent()!=null){
                int mState = getIntent().getIntExtra("state",0);
                if (mState==1) {
                    fragmentManager.beginTransaction().addToBackStack(NotificationFragment.class.getName()).replace(R.id.content, new NotificationFragment()).commit();
                }else {
                    fragmentManager.beginTransaction().addToBackStack(NowOrdersFragment.class.getName()).replace(R.id.content, new NowOrdersFragment()).commit();
                }
            }else {
            fragmentManager.beginTransaction().addToBackStack(NowOrdersFragment.class.getName()).replace(R.id.content, new NowOrdersFragment()).commit();
        }}
        else if (globalPreferences.getLoginAs().equals(Constant.benfiter)) {
            if (getIntent()!=null){
                int mState = getIntent().getIntExtra("state",0);
                if (mState==1) {
                    fragmentManager.beginTransaction().addToBackStack(NotificationFragment.class.getName()).replace(R.id.content, new NotificationFragment()).commit();
                }else {
                    fragmentManager.beginTransaction().addToBackStack(ServicesFragment.class.getName()).replace(R.id.content, new ServiceFragment()).commit();
                }
            }else {
                fragmentManager.beginTransaction().addToBackStack(ServicesFragment.class.getName()).replace(R.id.content, new ServiceFragment()).commit();
            }
        }
        else if (globalPreferences.getLoginAs().equals(Constant.visitor)) {
            iv_notification.setVisibility(View.GONE);
            fragmentManager.beginTransaction().addToBackStack(ServicesFragment.class.getName()).replace(R.id.content, new ServiceFragment()).commit();

        }
        navigationFragment = new NavigationFragment();
        fragmentManager.beginTransaction().replace(R.id.nav_view, navigationFragment).commit();
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputeType() {
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.navigate)
    void OnNavigation() {
        if (globalPreferences.getLanguage().equals("en")) {
            if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                drawerLayout.closeDrawer(Gravity.LEFT);
            } else {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        } else {
            if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
            } else {
                drawerLayout.openDrawer(Gravity.RIGHT);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == iv_notification) {
            getSupportFragmentManager().beginTransaction().addToBackStack(com.aait.seyana.Fragment.NotificationFragment.class.getName()).replace(R.id.content, new NotificationFragment()).commit();
        }
    }


    @Override
    public void onBackPressed() {
        Fragment frg = getSupportFragmentManager().findFragmentById(R.id.content);
        String frg_name = frg.getClass().getName();
        if (drawerLayout.isDrawerOpen(Gravity.LEFT) || drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            if (globalPreferences.getLanguage().equals("en"))
                drawerLayout.closeDrawer(Gravity.LEFT);
            else {
              //  if (globalPreferences.getLanguage().equals("ar"))
                    drawerLayout.closeDrawer(Gravity.RIGHT);
            }
        }
        if (globalPreferences.getLoginAs().equals(Constant.provider)) {
            if (frg.getClass().getName().equals(new NowOrdersFragment().getClass().getName())) {
                finish();
            } else if (frg_name.equals(new ProfileFragment().getClass().getName()) ||
                    frg_name.equals(new AboutFragment().getClass().getName()) ||
                    frg_name.equals(new SettingFragment().getClass().getName()) ||
                    frg_name.equals(new ContactFragment().getClass().getName()) ||
                    frg_name.equals(new TermsFragment().getClass().getName()) ||
                    frg_name.equals(new SuggestsFragment().getClass().getName())||
                    frg_name.equals(new Map().getClass().getName())||
                    frg_name.equals(new OrdersFragment().getClass().getName())||
                    frg_name.equals(new NotificationFragment().getClass().getName())||
                    frg_name.equals(new OrdersFragment().getClass().getName())||
                    frg_name.equals(new CongratulationSendFragment().getClass().getName())

                    ) {
                fragmentManager.popBackStack(NowOrdersFragment.class.getName(), 0);
            } else
                super.onBackPressed();
        }
        else if (globalPreferences.getLoginAs().equals(Constant.benfiter)) {
            if (frg.getClass().getName().equals(new ServiceFragment().getClass().getName())) {
                finish();
            } else if (frg_name.equals(new ProfileFragment().getClass().getName()) ||
                    frg_name.equals(new AboutFragment().getClass().getName()) ||
                    frg_name.equals(new SettingFragment().getClass().getName()) ||
                    frg_name.equals(new ContactFragment().getClass().getName()) ||
                    frg_name.equals(new TermsFragment().getClass().getName()) ||
                    frg_name.equals(new SuggestsFragment().getClass().getName()) ||
                    frg_name.equals(new CongratulationCompleteFragment().getClass().getName()) ||
                    frg_name.equals(new NotificationFragment().getClass().getName())||
                    frg_name.equals(new MyOrdersFragment().getClass().getName())||
                    frg_name.equals(new CongratulationSendFragment().getClass().getName())

                    ) {
                fragmentManager.popBackStack(ServicesFragment.class.getName(), 0);
            } else
                super.onBackPressed();
        }
        else if (globalPreferences.getLoginAs().equals(Constant.visitor)) {
            if (frg.getClass().getName().equals(new ServiceFragment().getClass().getName())) {
                finish();
            } else if (frg_name.equals(new LoginFragment().getClass().getName()) ||
                    frg_name.equals(new AboutFragment().getClass().getName()) ||
                    frg_name.equals(new SettingFragment().getClass().getName()) ||
                    frg_name.equals(new ContactFragment().getClass().getName()) ||
                    frg_name.equals(new CongratulationCompleteFragment().getClass().getName()) ||
                    frg_name.equals(new TermsFragment().getClass().getName())

                    ) {
                fragmentManager.popBackStack(ServicesFragment.class.getName(), 0);
            } else
                super.onBackPressed();
        }

    }

}


