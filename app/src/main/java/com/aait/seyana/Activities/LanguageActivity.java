package com.aait.seyana.Activities;
import android.content.Intent;
import android.widget.Button;

import com.aait.seyana.Base.ParentActivity;
import com.aait.seyana.R;

import butterknife.BindView;
import butterknife.OnClick;


public class LanguageActivity extends ParentActivity {
    @BindView(R.id.btn_arabic)
    Button btn_arabic;
    @BindView(R.id.btn_english)
    Button btn_english;
    @BindView(R.id.btn_ordo)
    Button btn_ordo;
    String lang="";


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_language;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected void init() {
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputeType() {
        return false;
    }

    @OnClick(R.id.btn_arabic)
    void Arabic()
    {
        lang="ar";
        getLanguage();
    }
    @OnClick(R.id.btn_english)
    void English()
    {
        lang="en";
        getLanguage();
    }
    @OnClick(R.id.btn_ordo)
    void Ordo()
    {
        lang="fa";
        getLanguage();
    }
    public void getLanguage() {
        globalPreferences.storeLanguage(lang);
        Intent mainAct = new Intent(LanguageActivity.this, LoginActivity.class);
        mainAct.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mainAct);
//        if (globalPreferences.getLoginStatus()){
//        CustomeProgressDialog.onStart(this, getString(R.string.please_wait));
//        RetroWeb.getClient().create(ServiceApi.class).getLanguage(lang)
//                .enqueue(new Callback<ChangeLangModel>() {
//
//                    @Override
//                    public void onResponse(Call<ChangeLangModel> call, Response<ChangeLangModel> response) {
//                        CustomeProgressDialog.onFinish();
//                        Util.onPrintLog(response.body());
//
//                        if (response.body().getValue().equals("1") ){
//                            CustomeProgressDialog.onFinish();
//                            globalPreferences.storeLanguage(response.body().getData());
//                            Intent mainAct = new Intent(LanguageActivity.this, LoginActivity.class);
//                            mainAct.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(mainAct);
//                           // finish();
//                        }
//                        else
//                        {
//                            CustomeProgressDialog.onFinish();
//
//                        }
//                    }
//
//
//                    @Override
//                    public void onFailure(Call<ChangeLangModel> call, Throwable t) {
//                        CustomeProgressDialog.onFinish();
//                        Util.onPrintLog(t.getMessage());
//                    }
//
//                });
    }

}
