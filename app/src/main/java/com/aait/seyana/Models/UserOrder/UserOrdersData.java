package com.aait.seyana.Models.UserOrder;

import com.google.gson.annotations.SerializedName;

public class UserOrdersData {
    @SerializedName("id")
    private int id;
    @SerializedName("service_name")
    private String service_name;
    @SerializedName("created_at")
    private String created_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
