package com.aait.seyana.Models.CompletedOrders;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aya on 10/24/2017.
 */

public class CompleteedOrderModel {
    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;
    @SerializedName("data")
    private List<CompletedOrderData> data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<CompletedOrderData> getData() {
        return data;
    }

    public void setData(List<CompletedOrderData> data) {
        this.data = data;
    }
}
