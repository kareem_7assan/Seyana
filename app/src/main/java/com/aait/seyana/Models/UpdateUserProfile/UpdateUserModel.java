package com.aait.seyana.Models.UpdateUserProfile;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 10/21/2017.
 */

public class UpdateUserModel {
    @SerializedName("value")
    private String value;
    @SerializedName("user_data")
    private UpdateUserData user_data;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public UpdateUserData getUser_data() {
        return user_data;
    }

    public void setUser_data(UpdateUserData user_data) {
        this.user_data = user_data;
    }
}
