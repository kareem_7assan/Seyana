package com.aait.seyana.Models.Suggests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 10/18/2017.
 */

public class SuggestsModel {
    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;
    @SerializedName("data")
    private SuggestsData data;
    @SerializedName("msg")
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public SuggestsData getData() {
        return data;
    }

    public void setData(SuggestsData data) {
        this.data = data;
    }
}
