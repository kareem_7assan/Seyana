package com.aait.seyana.Models.Profile;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 10/21/2017.
 */

public class ProfileModel {
    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;
    @SerializedName("user_data")
    private ProfileData user_data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ProfileData getUser_data() {
        return user_data;
    }

    public void setUser_data(ProfileData user_data) {
        this.user_data = user_data;
    }
}
