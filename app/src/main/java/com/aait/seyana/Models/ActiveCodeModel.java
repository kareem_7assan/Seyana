package com.aait.seyana.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 10/18/2017.
 */

public class ActiveCodeModel {
    @SerializedName("value")
    private String value;
    @SerializedName("key")
    private String key;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
