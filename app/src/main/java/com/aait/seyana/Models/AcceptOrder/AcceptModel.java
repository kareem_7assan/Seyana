package com.aait.seyana.Models.AcceptOrder;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 10/24/2017.
 */

public class AcceptModel {
    @SerializedName("value")
    private String value;
    @SerializedName("key")
    private String key;
    @SerializedName("data")
    private AcceptData data;
    @SerializedName("msg")
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public AcceptData getData() {
        return data;
    }

    public void setData(AcceptData data) {
        this.data = data;
    }
}
