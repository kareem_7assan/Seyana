package com.aait.seyana.Models.CompletedOrders;

import com.google.gson.annotations.SerializedName;

public class CompletedOrderData {
    @SerializedName("id")
    private int id;
    @SerializedName("user_name")
    private String user_name;
    @SerializedName("service_name")
    private String service_name;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("avatar")
    private String avatar;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
