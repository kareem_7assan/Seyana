package com.aait.seyana.Models.UserMakeOrder;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserOrderData {
    @SerializedName("id")
    private int id;
    @SerializedName("service")
    private String service;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lng")
    private String lng;
    @SerializedName("address")
    private String address;
    @SerializedName("description")
    private String description;
    @SerializedName("phone")
    private String phone;
    @SerializedName("verification_code")
    private int verification_code;
    @SerializedName("images")
    private List<UserImages> images;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getVerification_code() {
        return verification_code;
    }

    public void setVerification_code(int verification_code) {
        this.verification_code = verification_code;
    }

    public List<UserImages> getImages() {
        return images;
    }

    public void setImages(List<UserImages> images) {
        this.images = images;
    }
}
