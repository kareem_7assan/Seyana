package com.aait.seyana.Models.UpdateProviderProfile;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 10/21/2017.
 */

public class UpdateProviderModel {
    @SerializedName("value")
    private String value;
    @SerializedName("user_data")
    private UpdateProviderData user_data;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public UpdateProviderData getUser_data() {
        return user_data;
    }

    public void setUser_data(UpdateProviderData user_data) {
        this.user_data = user_data;
    }
}
