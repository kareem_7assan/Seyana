package com.aait.seyana.Models.Login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 10/18/2017.
 */

public class LoginModel {

    @SerializedName("value")
    private String value;
    @SerializedName("key")
    private String key;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("user")
    private LoginData user;
    @SerializedName("msg")
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public LoginData getUser() {
        return user;
    }

    public void setUser(LoginData user) {
        this.user = user;
    }
}
