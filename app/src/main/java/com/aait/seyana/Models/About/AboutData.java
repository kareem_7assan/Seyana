package com.aait.seyana.Models.About;

import com.google.gson.annotations.SerializedName;

public class AboutData {
    @SerializedName("about_app")
    private String about_app;

    public String getAbout_app() {
        return about_app;
    }

    public void setAbout_app(String about_app) {
        this.about_app = about_app;
    }
}
