package com.aait.seyana.Models.Notification;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mahmoud on 12/1/2017.
 */

public class NotificationModel {


    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;
    @SerializedName("data")
    private List<NotificationData> data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<NotificationData> getData() {
        return data;
    }

    public void setData(List<NotificationData> data) {
        this.data = data;
    }
}
