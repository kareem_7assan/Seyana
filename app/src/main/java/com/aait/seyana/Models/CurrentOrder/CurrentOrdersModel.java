package com.aait.seyana.Models.CurrentOrder;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aya on 10/24/2017.
 */

public class CurrentOrdersModel {
    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;
    @SerializedName("data")
    private List<CurrentOrderData> data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<CurrentOrderData> getData() {
        return data;
    }

    public void setData(List<CurrentOrderData> data) {
        this.data = data;
    }
}
