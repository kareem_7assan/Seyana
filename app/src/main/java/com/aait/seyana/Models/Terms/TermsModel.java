package com.aait.seyana.Models.Terms;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 10/18/2017.
 */

public class TermsModel {
    @SerializedName("value")
    private String value;
    @SerializedName("key")
    private String key;
    @SerializedName("data")
    private TermsData data;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public TermsData getData() {
        return data;
    }

    public void setData(TermsData data) {
        this.data = data;
    }
}
