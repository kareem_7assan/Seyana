package com.aait.seyana.Models.Contact;

import com.google.gson.annotations.SerializedName;

public class ContactData {
    @SerializedName("email")
    private String email;
    @SerializedName("number")
    private String number;
    @SerializedName("facebook")
    private String facebook;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }
}
