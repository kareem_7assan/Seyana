package com.aait.seyana.Models.UserMakeOrder;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 10/24/2017.
 */

public class UserMakeOrderModel {
    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;
    @SerializedName("data")
    private UserOrderData data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public UserOrderData getData() {
        return data;
    }

    public void setData(UserOrderData data) {
        this.data = data;
    }
}
