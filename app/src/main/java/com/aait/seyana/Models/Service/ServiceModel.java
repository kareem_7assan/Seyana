package com.aait.seyana.Models.Service;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aya on 10/18/2017.
 */

public class ServiceModel {
    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;
    @SerializedName("data")
    private List<ServiceData> data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<ServiceData> getData() {
        return data;
    }

    public void setData(List<ServiceData> data) {
        this.data = data;
    }
}
