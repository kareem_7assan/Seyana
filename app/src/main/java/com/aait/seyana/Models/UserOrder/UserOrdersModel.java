package com.aait.seyana.Models.UserOrder;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aya on 10/24/2017.
 */

public class UserOrdersModel {
    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;
    @SerializedName("data")
    private List<UserOrdersData> data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<UserOrdersData> getData() {
        return data;
    }

    public void setData(List<UserOrdersData> data) {
        this.data = data;
    }
}
