package com.aait.seyana.Models.Register;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 10/17/2017.
 */

public class RegisterModel {
    @SerializedName("value")
    private String value;
    @SerializedName("key")
    private String key;
    @SerializedName("user_id")
    private String user_id;

    @SerializedName("msg")
    private String msg;
    @SerializedName("user")
    private RegisterData user;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public RegisterData getUser() {
        return user;
    }

    public void setUser(RegisterData user) {
        this.user = user;
    }
}
