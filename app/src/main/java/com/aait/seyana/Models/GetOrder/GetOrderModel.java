package com.aait.seyana.Models.GetOrder;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 10/25/2017.
 */

public class GetOrderModel {
    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;
    @SerializedName("msg")
    private String msg;
    @SerializedName("data")
    private GetOrderData data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public GetOrderData getData() {
        return data;
    }

    public void setData(GetOrderData data) {
        this.data = data;
    }
}
