package com.aait.seyana.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 10/18/2017.
 */

public class ChangePasswordModel {
    @SerializedName("value")
    private String value;
    @SerializedName("msg")
    private String msg;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
