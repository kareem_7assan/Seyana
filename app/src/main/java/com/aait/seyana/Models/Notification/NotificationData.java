package com.aait.seyana.Models.Notification;

import com.google.gson.annotations.SerializedName;

public class NotificationData {
    @SerializedName("id")
    private int id;
    @SerializedName("content")
    private String content;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("nextId")
    private int nextId;
    @SerializedName("key")
    private String key;

    public int getNextId() {
        return nextId;
    }

    public void setNextId(int nextId) {
        this.nextId = nextId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
