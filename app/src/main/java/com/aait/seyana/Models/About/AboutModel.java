package com.aait.seyana.Models.About;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 10/18/2017.
 */

public class AboutModel {
    @SerializedName("value")
    private String value;
    @SerializedName("key")
    private String key;
    @SerializedName("data")
    private AboutData data;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public AboutData getData() {
        return data;
    }

    public void setData(AboutData data) {
        this.data = data;
    }
}
