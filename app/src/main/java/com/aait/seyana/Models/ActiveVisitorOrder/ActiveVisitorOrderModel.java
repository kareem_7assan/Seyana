package com.aait.seyana.Models.ActiveVisitorOrder;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aya on 10/24/2017.
 */

public class ActiveVisitorOrderModel {
    @SerializedName("value")
    private String value;
    @SerializedName("key")
    private String key;
    @SerializedName("data")
    private ActiveVisitorData data;
    @SerializedName("msg")
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ActiveVisitorData getData() {
        return data;
    }

    public void setData(ActiveVisitorData data) {
        this.data = data;
    }
}
