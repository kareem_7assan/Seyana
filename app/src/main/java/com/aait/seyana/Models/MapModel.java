package com.aait.seyana.Models;

/**
 * Created by Aya on 10/15/2017.
 */

public class MapModel {
    private  String lat;
    private  String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
